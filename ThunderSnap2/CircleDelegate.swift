//
//  CircleDelegate.swift
//  ThunderSnap2
//
//  Created by Nikola on 28/03/2018.
//  Copyright © 2018 Nikola Bozhkov. All rights reserved.
//

import Foundation

protocol CircleDelegate {
    func didConsume(circle: Circle)
}
