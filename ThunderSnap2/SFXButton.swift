//
//  SFXButton.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 4/14/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class SFXButton: BlockButton {
    
    let sfxOnString = "SOUNDS ON"
    let sfxOffString = "SOUNDS OFF"
    
    init(size: CGSize, fontSize: CGFloat = BlockButton.defaultFontSize, cornerRadius: CGFloat = BlockButton.defaultCornerRadius) {
        super.init(text: "", size: size, fontSize: fontSize, cornerRadius: cornerRadius)
        update()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func toggle() {
        SoundManager.shared.isSoundEnabled = !SoundManager.shared.isSoundEnabled
        update()
    }
    
    func update() {
        if SoundManager.shared.isSoundEnabled {
            self.label.text = self.sfxOnString
            self.fillColor = .white
        } else {
            self.label.text = self.sfxOffString
            self.fillColor = .gray
        }
    }
}
