//
//  GameOverScene.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 3/8/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class GameOverScene: SKScene {
    
    let margin: CGFloat = 25
    
    var stats: EndGameStatsModel!
    
    var replayBtn: SwirlButton!
    var homeBtn: SwirlButton!
    var skillsBtn: SwirlButton!
    var gameCenterBtn: SwirlButton!
    var currencyLabel: SKLabelNode!
    
    override func didMove(to view: SKView) {
        
        let font = Constants.mainFont
        let fontSize: CGFloat = 70
        
        let leftX: CGFloat = 282
        let rightX = self.frame.width - leftX
        let topY: CGFloat = 520
        let duration: CGFloat = 1.0
        let placeHolderText = "0123456789"
        
        let scoreCurrency = Int(CGFloat(stats.score) * 0.2)
        let comboCurrency = stats.biggestCombo
        var totalCurrency = scoreCurrency + comboCurrency
        
        // Give 1 currency if any points are made and still earned 0
        if totalCurrency == 0 && stats.score != 0 {
            totalCurrency = 1
        }
        
        stats.currency = totalCurrency
        self.saveGameData()
        
        let gameOverLabel = SKLabelNode(fontNamed: font)
        gameOverLabel.fontSize = 90
        gameOverLabel.fontColor = Constants.dangerRedColor
        gameOverLabel.verticalAlignmentMode = .top
        gameOverLabel.horizontalAlignmentMode = .center
        gameOverLabel.text = "GAME OVER"
        gameOverLabel.position = CGPoint(x: self.frame.midX, y: 705)
        self.addChild(gameOverLabel)
        
        // MARK: Score
        let scoreTextLabel = SKLabelNode(fontNamed: font)
        scoreTextLabel.fontSize = fontSize
        scoreTextLabel.fontColor = Category.blue.saturated
        scoreTextLabel.horizontalAlignmentMode = .left
        scoreTextLabel.verticalAlignmentMode = .bottom
        scoreTextLabel.text = "SCORE"
        scoreTextLabel.position = CGPoint(x: leftX, y: topY)
        scoreTextLabel.zRotation = .pi * 25 / 180 // Start rotated 25 degrees
        scoreTextLabel.run(SKAction(named: "BounceAngleAction")!)
        self.addChild(scoreTextLabel)
        
        let scoreLabel = SKLabelNode(fontNamed: font)
        scoreLabel.fontSize = fontSize
        scoreLabel.fontColor = .white
        scoreLabel.horizontalAlignmentMode = .left
        scoreLabel.verticalAlignmentMode = .top
        scoreLabel.text = placeHolderText
        scoreLabel.position = scoreTextLabel.position.offsetted(dx: 0, dy: -margin)
        try! scoreLabel.animate(start: 0, delta: stats.score, timingMode: .easeOut, format: "(*)", duration: duration)
        self.addChild(scoreLabel)
        
        // MARK: Best Score
        let bestScoreTextLabel = SKLabelNode(fontNamed: font)
        bestScoreTextLabel.fontSize = fontSize
        bestScoreTextLabel.fontColor = Category.orange.saturated
        bestScoreTextLabel.horizontalAlignmentMode = .left
        bestScoreTextLabel.verticalAlignmentMode = .top
        bestScoreTextLabel.text = "BEST"
        bestScoreTextLabel.position = scoreLabel.position.offsetted(dx: 0, dy: -margin * 2 - scoreLabel.frame.height)
        self.addChild(bestScoreTextLabel)
        let bestScoreLabel = SKLabelNode(fontNamed: font)
        bestScoreLabel.fontSize = fontSize
        bestScoreLabel.fontColor = .white
        bestScoreLabel.horizontalAlignmentMode = .left
        bestScoreLabel.verticalAlignmentMode = .top
        bestScoreLabel.text = String(GameData.sharedInstance.highscore)
        bestScoreLabel.position = bestScoreTextLabel.position.offsetted(dx: 0, dy: -margin - bestScoreTextLabel.frame.height)
        self.addChild(bestScoreLabel)
        
        // MARK: Combo
        let comboTextLabel = SKLabelNode(fontNamed: font)
        comboTextLabel.fontSize = fontSize
        comboTextLabel.fontColor = Category.green.saturated
        comboTextLabel.horizontalAlignmentMode = .right
        comboTextLabel.verticalAlignmentMode = .bottom
        comboTextLabel.text = "COMBO"
        comboTextLabel.position = CGPoint(x: rightX, y: topY)
        self.addChild(comboTextLabel)
        
        let comboLabel = SKLabelNode(fontNamed: font)
        comboLabel.fontSize = fontSize
        comboLabel.fontColor = .white
        comboLabel.horizontalAlignmentMode = .right
        comboLabel.verticalAlignmentMode = .top
        comboLabel.position = comboTextLabel.position.offsetted(dx: 0, dy: -margin)
        try! comboLabel.animate(start: 0, delta: stats.biggestCombo, timingMode: .easeOut, format: "(*)", duration: duration)
        self.addChild(comboLabel)
        
        // MARK: Color Disks Earned
        let currencyTextLabel = SKLabelNode(fontNamed: font)
        currencyTextLabel.fontSize = fontSize
        currencyTextLabel.fontColor = Category.yellow.saturated
        currencyTextLabel.horizontalAlignmentMode = .right
        currencyTextLabel.verticalAlignmentMode = .top
        currencyTextLabel.text = "EARNED"
        currencyTextLabel.position = CGPoint(x: rightX, y: bestScoreTextLabel.position.y)
        self.addChild(currencyTextLabel)
        
        currencyLabel = SKLabelNode(fontNamed: font)
        currencyLabel.fontSize = fontSize
        currencyLabel.fontColor = .white
        currencyLabel.horizontalAlignmentMode = .right
        currencyLabel.verticalAlignmentMode = .top
        currencyLabel.position = CGPoint(x: rightX, y: bestScoreLabel.position.y)
        try! currencyLabel.animate(start: 0, delta: totalCurrency, timingMode: .easeOut, format: "(*)", duration: duration)
        self.addChild(currencyLabel)

        bestScoreLabel.text = String(GameData.sharedInstance.highscore)
        
        // MARK: Buttons
        // Replay button
        let replayRadius: CGFloat = 90
        replayBtn = SwirlButton(iconTexture: Resources.shared.replayIconTexture, radius: replayRadius)
        replayBtn.position = self.pointFromBottom(CGPoint(x: self.frame.midX, y: 130))
        self.addChild(replayBtn)
        
        // Home button
        let homeRadius: CGFloat = 65
        homeBtn = SwirlButton(iconTexture: Resources.shared.homeIconTexture, radius: homeRadius, iconScale: 0.7)
        homeBtn.position = self.pointFromTop(CGPoint(x: 40 + homeRadius, y: 40 + homeRadius))
        self.addChild(homeBtn)
        
        // Game Center button
        gameCenterBtn = SwirlButton(iconTexture: Resources.shared.gameCenterIconTexture)
        gameCenterBtn.position = Util.getButtonPosition(centerBtn: replayBtn, number: 1)
        self.addChild(gameCenterBtn)
        
        // MARK: Lightning effects
        let threadMargin: CGFloat = 30
        let xLeft = leftX - threadMargin,
        xRight = rightX + threadMargin,
        yTopStart = comboTextLabel.position.y + comboTextLabel.frame.height,
        yTopEnd = bestScoreTextLabel.position.y + margin * 2,
        yBotStart = bestScoreTextLabel.position.y,
        yBotEnd = bestScoreLabel.position.y - bestScoreLabel.frame.height,
        speedFactor: CGFloat = 0.5,
        spawnInterval: CGFloat = 0.14
        
        let scoreThread = EternalLightning(startPoint: CGPoint(x: xLeft, y: yTopStart),
                                        endPoint: CGPoint(x: xLeft, y: yTopEnd),
                                        interval: spawnInterval,
                                        speedFactor: speedFactor,
                                        color: Category.blue.lightened)
        let bestThread = EternalLightning(startPoint: CGPoint(x: xLeft, y: yBotStart),
                                      endPoint: CGPoint(x: xLeft, y: yBotEnd),
                                      interval: spawnInterval,
                                      speedFactor: speedFactor,
                                      color: Category.orange.lightenedTwo)
        let comboThread = EternalLightning(startPoint: CGPoint(x: xRight, y: yTopStart),
                                        endPoint: CGPoint(x: xRight, y: yTopEnd),
                                        interval: spawnInterval,
                                        speedFactor: speedFactor,
                                        color: Category.green.lightened)
        let goldThread = EternalLightning(startPoint: CGPoint(x: xRight, y: yBotStart),
                                       endPoint: CGPoint(x: xRight, y: yBotEnd),
                                       interval: spawnInterval,
                                       speedFactor: speedFactor,
                                       color: Category.yellow.lightened)
        
        self.addChild(scoreThread)
        self.addChild(bestThread)
        self.addChild(comboThread)
        self.addChild(goldThread)
    }
    
    func saveGameData() {
        if stats.score > GameData.sharedInstance.highscore {
            GameData.sharedInstance.highscore = stats.score
            GameCenterManager.reportHighscore(stats.score)
        }
        
        if stats.biggestCombo > GameData.sharedInstance.biggestCombo {
            GameData.sharedInstance.biggestCombo = stats.biggestCombo
            GameCenterManager.reportBiggestCombo(stats.biggestCombo)
        }
        
        GameData.sharedInstance.currency += stats.currency
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let location = touches.first!.location(in: self)
        
        if replayBtn.contains(location) {
            SoundManager.playSFXAction(Resources.shared.btnClickSFXAction, node: self)
            Util.presentScene(MainScene(), view: self.view!)
            
        } else if homeBtn.contains(location) {
            SoundManager.playSFXAction(Resources.shared.btnClickSFXAction, node: self)
            Util.presentScene(HomeScene(), view: self.view!)
            
        } else if gameCenterBtn.contains(location) {
            SoundManager.playSFXAction(Resources.shared.btnClickSFXAction, node: self)
            NotificationCenter.default.post(name: .showLeaderboard, object: nil)
        }
    }
    
    override func willMove(from view: SKView) {
        NotificationCenter.default.removeObserver(self)
    }
}
