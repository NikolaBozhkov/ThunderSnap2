//
//  SoundManager.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 4/12/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit
import AVFoundation

class SoundManager {
    
    static let shared = SoundManager()
    
    let bgAudioVolume: Float = 0.10
    
    var bgAudioPlayer: AVAudioPlayer?
    
    var isSoundEnabled: Bool {
        didSet {
            UserDefaults.standard.set(isSoundEnabled, forKey: Keys.soundEnabled)
        }
    }
    
    var isMusicEnabled: Bool {
        didSet {
            bgAudioPlayer?.volume = isMusicEnabled ? bgAudioVolume : 0
            UserDefaults.standard.set(isMusicEnabled, forKey: Keys.musicEnabled)
        }
    }
    
    var shouldPlay = false
    
    private init() {
        isSoundEnabled = UserDefaults.standard.bool(forKey: Keys.soundEnabled)
        isMusicEnabled = UserDefaults.standard.bool(forKey: Keys.musicEnabled)
        
        if !UserDefaults.standard.bool(forKey: Keys.notFirstBootUp) {
            isSoundEnabled = true
            isMusicEnabled = true
        }
        
        guard let path = Bundle.main.path(forResource: "sickestBackground", ofType: "mp3") else { return }
        let url = URL(fileURLWithPath: path)
        
        do {
            try bgAudioPlayer = AVAudioPlayer(contentsOf: url)
            bgAudioPlayer!.volume = isMusicEnabled ? bgAudioVolume : 0
            bgAudioPlayer!.numberOfLoops = -1
//            bgAudioPlayer!.prepareToPlay()
        } catch {
            print("failed to load audioPlayer")
        }
    }
    
    class func playSFXAction(_ action: SKAction, node: SKNode) {
        if shared.isSoundEnabled {
            node.run(action)
        }
    }
}
