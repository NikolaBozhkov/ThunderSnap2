//
//  MainScene.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 10/29/16.
//  Copyright © 2016 Nikola Bozhkov. All rights reserved.
//

import Foundation
import SpriteKit

class MainScene: SKScene, SKPhysicsContactDelegate, CircleDelegate {
    
    var timeLeftMax: TimeInterval = 20
    var timeLeft: TimeInterval = 20
    var timeGainOnConsume: TimeInterval = Spawner.defaultSpawnInterval
    var timeBar: SKSpriteNode!
    
    var diagonalLength: CGFloat!
    var touchBeganLocation: CGPoint?
    
    var isDragEnabled = true
    var isGamePaused = false {
        didSet {
            self.gameNode.isPaused = isGamePaused
        }
    }

    var lastTime: TimeInterval = 0
    var statsNode = StatsNode()
    var spawner: Spawner!
    let gameNode = SKNode()
    
    var circles = Set<Circle>()
    var selectedCircle: Circle?
    var prevSelectedCircle: Circle? // Prevents drag selection and tap selection interference
    
    var isGameOver: Bool {
        return timeLeft <= 0
    }
    
    override public func didMove(to view: SKView) {
        spawner = Spawner(scene: self)
        
        gameNode.position = .zero
        addChild(gameNode)
        physicsWorld.contactDelegate = self
        physicsWorld.gravity = .zero
        diagonalLength = sqrt(pow(size.width, 2) + pow(size.height, 2))
        
        statsNode.position = pointFromTop(CGPoint(x: frame.midX, y: 20))
        statsNode.zPosition = Constants.statsNodeZPos
        gameNode.addChild(statsNode)
        
        timeBar = SKSpriteNode(texture: nil, color: .white, size: CGSize(width: frame.width, height: 10))
        timeBar.position = pointFromTop(.zero)
        timeBar.anchorPoint = CGPoint(x: 0, y: 1)
        gameNode.addChild(timeBar)
        
        // Game state notifications
        NotificationCenter.default.addObserver(self, selector: #selector(pause), name: .UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(unpause), name: .resumeGame, object: nil)
        
        // Gestures
        let threeFingerTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(pause))
        threeFingerTapRecognizer.numberOfTouchesRequired = 3
        view.addGestureRecognizer(threeFingerTapRecognizer)
        
        // Add walls
        func createWall(size: CGSize) -> SKSpriteNode {
            let wall = SKSpriteNode(color: .white, size: size)
            wall.physicsBody = SKPhysicsBody(rectangleOf: wall.size)
            wall.physicsBody!.isDynamic = false
            self.addChild(wall)
            return wall
        }
        
        let leftWall = createWall(size: CGSize(width: 1, height: Constants.gameAreaHeight))
        leftWall.position = CGPoint(x: -leftWall.frame.width / 2, y: Constants.gameAreaHeight / 2)
        
        let rightWall = createWall(size: CGSize(width: 1, height: Constants.gameAreaHeight))
        rightWall.position = CGPoint(x: Constants.gameAreaWidth + rightWall.frame.width / 2, y: Constants.gameAreaHeight / 2)
        
        let topWall = createWall(size: CGSize(width: Constants.gameAreaWidth, height: 1))
        topWall.position = CGPoint(x: Constants.gameAreaWidth / 2, y: Constants.gameAreaHeight + topWall.frame.height / 2)
        
        
        let bottomWall = createWall(size: CGSize(width: Constants.gameAreaWidth, height: 1))
        bottomWall.position = CGPoint(x: Constants.gameAreaWidth / 2, y: -bottomWall.frame.height / 2)
        
        spawner.spawnStartingCircles()
    }
    
    override public func update(_ currentTime: TimeInterval) {
        let deltaT = lastTime == 0.0 ? 1.0 / 60 : currentTime - lastTime
        guard !isGamePaused else {
            self.gameNode.isPaused = true
            return
        }
        
        timeLeft -= deltaT
        timeLeft = max(timeLeft, 0)
        timeBar.xScale = CGFloat(timeLeft / timeLeftMax)
        
        if isGameOver {
            let scene = Util.loadScene(GameOverScene(), view: self.view!) as! GameOverScene
            scene.stats = statsNode.getEndGameStatsModel()
            Util.presentScene(scene, view: view!)
        }
        
        spawner.update(deltaT: deltaT)
        circles.forEach { $0.update(deltaT: deltaT) }
        
        lastTime = currentTime
    }
    
    override func didFinishUpdate() {
        circles.forEach { $0.didUpdate() }
    }
    
    @objc func pause() {
        guard !self.isGamePaused else { return }
        self.lastTime = 0
        self.isGamePaused = true
        
        let pauseOverlay = PauseOverlay(size: self.frame.size)
        pauseOverlay.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        self.addChild(pauseOverlay)
    }
    
    @objc func unpause() {
        self.isGamePaused = false
    }
    
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let location = touches.first!.location(in: self)
        self.touchBeganLocation = location
        
        let circle = self.getCircle(atPoint: location, excludeSelected: false)
        
        if let circle = circle {
            SoundManager.playSFXAction(Resources.shared.circleSelectSFXAction, node: self)
            
            // If there is a circle below the selected one, select it instead
            if self.selectedCircle === circle {
                let circleBelow = self.getCircle(atPoint: location, excludeSelected: true)
                
                if let circleBelow = circleBelow {
                    self.selectCircle(circleBelow)
                    return
                }
            }
            
            self.selectCircle(circle)
            
            // Enable drag to target option
            self.isDragEnabled = true
        }
        else {
            self.isDragEnabled = false
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let location = touches.first!.location(in: self)
        
        // If dragging towards some target, haven't lifted the finger and actually changed touch position
        if let selectedCircle = self.selectedCircle,
            self.isDragEnabled, self.touchBeganLocation != location {
            
            // If released outside of the selected circle -> targeting another circle
            if location.distanceTo(selectedCircle.position) > Resources.shared.circleRadius {
                
                // Get the normal delta vector of the touch and the selected circle
                let normalSlopeTouch = (selectedCircle.position - location).normalized()
                
                // Iterate all circles without disabled and selected
                var circleFactors: [(circle: Circle, factor: CGFloat)] = []
                for circle in circles.filter({$0 != self.selectedCircle}) {
                    
                    // Get the normal delta vector of the circle and the selected circle
                    let normalSlopeCircle = (selectedCircle.position - circle.position).normalized()
                    
                    // The slope factor is how close the circle slope normal is to the touch slope normal
                    let slopeFactor = (normalSlopeTouch - normalSlopeCircle).length()
                    
                    // The distance factor is the fraction of the touch to circle of max distance
                    let distanceFactor = location.distanceTo(circle.position) / self.diagonalLength
                    
                    circleFactors.append((circle, slopeFactor + distanceFactor))
                }
                
                if let closest = circleFactors.min(by: {$0.factor < $1.factor}) {
                    self.selectCircle(closest.circle)
                }
            }
            else {
                // Drag is disabled because touch ended inside circle
                self.isDragEnabled = false
            }
        }
    }
    
    // MARK: Circle Management
    func getCircle(atPoint point: CGPoint, excludeSelected: Bool, bonusRadius: CGFloat = 15) -> Circle? {
        
        for circle in circles {
            let distance = point.distanceTo(circle.position)
            let isSelected = excludeSelected ? circle == selectedCircle : false
            
            if !isSelected && distance <= circle.radius + bonusRadius {
                return circle
            }
        }
        
        return nil
    }
    
    func selectCircle(_ circle: Circle) {
        if let selectedCircle = self.selectedCircle {
            // If not targeting self
            if selectedCircle !== circle {
                
                // Follow the clicked ball and add follower
                selectedCircle.set(target: circle)
                self.prevSelectedCircle = circle
                SoundManager.playSFXAction(Resources.shared.circleTargetSFXAction, node: self)
            }
            else if selectedCircle === circle && circle.target != nil {
                // If targeting self stop following and remove follower
                circle.clearTarget()
                self.prevSelectedCircle = selectedCircle
            }
            
            // Go to default unselected state
            self.selectedCircle = nil
        }
        else {
            selectedCircle = circle
        }
    }
    
    // MARK: Circle Delegate Methods
    func didConsume(circle: Circle) {
        circles.remove(circle)
        circle.removeFromParent()
        
        // Clear selected circle if consumed
        if circle === selectedCircle {
            selectedCircle = nil
        }
        
        // Run explosion effect
        let emitter = Resources.shared.circleExplosionEmitter.copy() as! SKEmitterNode
        emitter.position = circle.position
        emitter.zPosition = Constants.mergeEffectZPos
        emitter.particleColor = circle.colorType.lightened
        
        emitter.run(SKAction.sequence([
            SKAction.wait(forDuration: TimeInterval(emitter.particleLifetime)),
            SKAction.removeFromParent()]))
        gameNode.addChild(emitter)
        SoundManager.playSFXAction(Resources.shared.mergeSFXAction, node: gameNode)
        
        if !isGameOver {
            timeLeft += timeGainOnConsume
            timeLeft.clamp(0, timeLeftMax)
            statsNode.score += 1
        }
    }
}
