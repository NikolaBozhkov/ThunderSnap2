//
//  EndGameStatsModel.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 3/8/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import Foundation

struct EndGameStatsModel {
    
    var score: Int
    var biggestCombo: Int
    var currency: Int = 0
    
    init(score: Int, biggestCombo: Int) {
        self.score = score
        self.biggestCombo = biggestCombo
    }
}
