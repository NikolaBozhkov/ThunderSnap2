//
//  GameData.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 3/18/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import Foundation

struct Keys {
    static let highscore = "highscore"
    static let currency = "currency"
    static let biggestCombo = "biggestCombo"
    static let skillPoints = "skillPoints"
    static let currentMergesForSkillPoint = "currentMergesForSkillPoint"
    
    static let spawnDelaySkillLevel = "spawnDelaySkillLevel"
    static let fireRadiusSkillLevel = "fireRadiusSkillLevel"
    static let fireDurationSkillLevel = "fireDurationSkillLevel"
    static let impactFactorSkillLevel = "impactFactorSkillLevel"
    static let overloadDurationSkillLevel = "overloadDurationSkillLevel"
    static let overloadRadiusSkillLevel = "overloadRadiusSkillLevel"
    
    static let tutorialCompleted = "tutorialCompleted"
    static let gcScoreUpToDate = "gcScoreUpToDate"
    static let gcComboUpToDate = "gcComboUpToDate"
    static let rewardVideoLastTime = "rewardVideoLastTime"
    static let notFirstBootUp = "notFirstBootUp"
    static let pauseOption = "pauseOption"
    static let musicEnabled = "musicEnabled"
    static let soundEnabled = "soundEnabled"
}

class GameData {
    static let sharedInstance = GameData()
    
    var highscore: Int {
        didSet {
            UserDefaults.standard.set(highscore, forKey: Keys.highscore)
        }
    }
    
    var currency: Int {
        didSet {
            UserDefaults.standard.set(currency, forKey: Keys.currency)
            NotificationCenter.default.post(name: .currencyChanged, object: nil)
        }
    }
    
    var skillPoints: Int {
        didSet {
            UserDefaults.standard.set(skillPoints, forKey: Keys.skillPoints)
            NotificationCenter.default.post(name: .skillPointsChanged, object: nil)
        }
    }
    
    var currentMergesForSkillPoint: Int {
        didSet {
            UserDefaults.standard.set(currentMergesForSkillPoint, forKey: Keys.currentMergesForSkillPoint)
        }
    }
    
    var biggestCombo: Int {
        didSet {
            UserDefaults.standard.set(biggestCombo, forKey: Keys.biggestCombo)
        }
    }
    
    private init() {
        self.highscore = UserDefaults.standard.integer(forKey: Keys.highscore)
        self.currency = UserDefaults.standard.integer(forKey: Keys.currency)
        self.biggestCombo = UserDefaults.standard.integer(forKey: Keys.biggestCombo)
        self.skillPoints = UserDefaults.standard.integer(forKey: Keys.skillPoints)
        self.currentMergesForSkillPoint = UserDefaults.standard.integer(forKey: Keys.currentMergesForSkillPoint)
        self.skillPoints = 100
    }
}
