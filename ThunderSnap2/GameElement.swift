//
//  GameElement.swift
//  Attraction
//
//  Created by Nikola on 22/09/2017.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

protocol GameElement {
    
    var position: CGPoint { get set }
    
    func remove(fromScene scene: MainScene)
}
