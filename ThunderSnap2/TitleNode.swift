//
//  TitleNode.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 4/7/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class TitleNode: SKSpriteNode {
    
    init() {
        
        super.init(texture: Resources.shared.titleBaseTexture, color: .clear, size: Resources.shared.titleBaseTexture.size())
        self.blendMode = .add
        
        let angle = CGFloat.pi * 1.5 / 180
        self.zRotation = -angle
        let duration: TimeInterval = 1
        
        let rotateLeft = SKAction.rotate(toAngle: angle, duration: duration)
        rotateLeft.timingMode = .easeOut
        let rotateRight = SKAction.rotate(toAngle: -angle, duration: duration)
        rotateRight.timingMode = .easeOut
        self.run(SKAction.repeatForever(SKAction.sequence([rotateLeft, rotateRight])))
        
        let scaleUp = SKAction.scale(to: 1.05, duration: duration / 2)
        let scaleDown = SKAction.scale(to: 1, duration: duration / 2)
        scaleUp.timingMode = .easeOut
        scaleDown.timingMode = .easeOut
        self.run(SKAction.repeatForever(SKAction.sequence([scaleUp, scaleDown])))
        
        let alphaTop: CGFloat = 1
        let alphaLow: CGFloat = 0.5
        let glow = SKSpriteNode(texture: Resources.shared.titleGlowTexture)
        glow.alpha = alphaLow
        
        let glowAlphaUp = SKAction.fadeAlpha(to: alphaTop, duration: scaleUp.duration)
        let glowAlphaDown = SKAction.fadeAlpha(to: alphaLow, duration: scaleDown.duration)
        glowAlphaUp.timingMode = scaleUp.timingMode
        glowAlphaDown.timingMode = scaleDown.timingMode
        glow.run(SKAction.repeatForever(SKAction.sequence([glowAlphaUp, glowAlphaDown])))
        
        self.zPosition = 2
        glow.zPosition = 1
        
        glow.position = .zero
        self.addChild(glow)
        
        self.setScale(1)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
