//
//  BlockButton.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 4/14/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class BlockButton: SKShapeNode {

    static let defaultFontSize: CGFloat = 30
    static let defaultCornerRadius: CGFloat = 5
    let label: SKLabelNode
    
    init(text: String, size: CGSize, fontSize: CGFloat = BlockButton.defaultFontSize, cornerRadius: CGFloat = BlockButton.defaultCornerRadius) {
        self.label = SKLabelNode(fontNamed: Constants.mainFont)
        self.label.verticalAlignmentMode = .center
        self.label.horizontalAlignmentMode = .center
        self.label.position = .zero
        self.label.fontColor = Constants.bgColor
        self.label.zPosition = 1
        self.label.fontSize = fontSize
        self.label.text = text
        
        super.init()
        self.path = CGPath(roundedRect: CGRect.init(origin: CGPoint(x: -size.width / 2, y: -size.height / 2), size: size),
                           cornerWidth: cornerRadius,
                           cornerHeight: cornerRadius,
                           transform: nil)
        self.fillColor = .white
        self.lineWidth = 0
        
        self.addChild(self.label)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
