//
//  Consumer.swift
//  Attraction
//
//  Created by Nikola on 21/09/2017.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import Foundation

protocol Consumer {
    
    associatedtype Consumable
    func consume(_ element: Consumable)
}
