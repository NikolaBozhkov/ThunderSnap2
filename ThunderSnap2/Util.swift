//
//  Util.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 11/23/16.
//  Copyright © 2016 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

public class Util {
    class func loadScene(named: String, view: SKView) -> SKScene {
        let scene = SKScene(fileNamed: named)!
        return loadScene(scene, view: view, fromFile: true)
    }
    
    class func loadScene(_ scene: SKScene, view: SKView, fromFile: Bool = false) -> SKScene {
        if !fromFile {
            scene.size = CGSize(width: 1334, height: 750)
            scene.backgroundColor = Constants.bgColor
        }
        
        scene.scaleMode = .aspectFit
        
        // Extends the scene if the ratio of the view is 3:4(iPads)
        let newHeight = view.bounds.height / view.bounds.width * scene.frame.width
        
        if newHeight > scene.frame.height {
            scene.anchorPoint = CGPoint(x: 0, y: (newHeight - scene.frame.height) / 2 / newHeight)
            //let oldHeight = scene.frame.height
            scene.size = CGSize(width: scene.frame.width, height: newHeight)
            
//            if scene is MainScene {
//                let height = (newHeight - oldHeight) / 2
//                let backgroundTop = SKSpriteNode(color: scene.backgroundColor,
//                                                 size: CGSize(width: scene.frame.width, height: height))
//                backgroundTop.zPosition = 99
//                
//                let backgroundBottom = backgroundTop.copy() as! SKSpriteNode
//                
//                backgroundTop.anchorPoint = CGPoint(x: 0, y: 1)
//                backgroundTop.position = CGPoint(x: 0, y: newHeight - height)
//                backgroundBottom.anchorPoint = .zero
//                backgroundBottom.position = CGPoint(x: 0, y: -height - 1)
//                
//                scene.addChild(backgroundTop)
//                scene.addChild(backgroundBottom)
//            }
        }
        
        return scene
    }
    
    class func presentScene(named: String, view: SKView) {
        let scene = loadScene(named: named, view: view)
        view.presentScene(scene, transition: .fade(withDuration: 0.5))
    }
    
    class func presentScene(_ scene: SKScene, view: SKView) {
        let scene = loadScene(scene, view: view)
        view.presentScene(scene, transition: .fade(withDuration: 0.5))
    }
    
    class func getButtonPosition(centerBtn: SwirlButton, number: CGFloat, radius: CGFloat = Constants.btnRadius) -> CGPoint {
        let distance = centerBtn.radius + radius + (abs(number) - 1) * radius * 2
        let margins = abs(number) * Constants.btnMargin
        
        let direction: CGFloat = number < 0 ? -1 : 1
        
        return centerBtn.position.offsetted(dx: direction * (distance + margins), dy: radius - centerBtn.radius)
    }
}
