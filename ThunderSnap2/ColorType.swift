//
//  ColorType.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 12/21/16.
//  Copyright © 2016 Nikola Bozhkov. All rights reserved.
//

import UIKit

struct ColorType: Hashable {
    let base: UIColor
    let lightened: UIColor
    let lightenedTwo: UIColor
    let saturated: UIColor
    
    let bitMask: UInt32
    
    var hashValue: Int {
        return bitMask.hashValue
    }
    
    init(base: UIColor, lightened: UIColor, lightenedTwo: UIColor, saturated: UIColor, bitMask: UInt32) {
        self.base = base
        self.lightened = lightened
        self.lightenedTwo = lightenedTwo
        self.saturated = saturated
        self.bitMask = bitMask
    }
    
    static func == (left: ColorType, right: ColorType) -> Bool {
        return left.bitMask == right.bitMask
    }
}
