//
//  Category.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 11/15/16.
//  Copyright © 2016 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

struct Category {
//    static let typeA = getColorType(base: "F45E30", saturated: 0.2, lightened: 0.1, lightenedTwo: 0.3, bitMask: 0b1)
//    static let typeB = getColorType(base: "FFEA32", saturated: 0.2, lightened: 0.1, lightenedTwo: 0.3, bitMask: 0b10)
//    static let typeC = getColorType(base: "2FB4ED", saturated: 0.2, lightened: 0.1, lightenedTwo: 0.3, bitMask: 0b100)
//    static let typeD = getColorType(base: "95C427", saturated: 0.2, lightened: 0.1, lightenedTwo: 0.3, bitMask: 0b1000)
    static let blue = getColorType(base: "2BC4D8", saturated: "00E1FF", lightened: "3FE8FF", lightenedTwo: "72EEFF", bitMask: 0b1)
    static let green = getColorType(base: "87D82B", saturated: "88FF00", lightened: "A5FF3F", lightenedTwo: "BDFF72", bitMask: 0b10)
    static let orange = getColorType(base: "FF7200", saturated: "FF7200", lightened: "FF8019", lightenedTwo: "FF8E32", bitMask: 0b100)
    static let yellow = getColorType(base: "FFEA32", saturated: "FFE500", lightened: "FFEB3A", lightenedTwo: "FFF072", bitMask: 0b1000)
    static let white = getColorType(base: "FFFFFF", saturated: "FFFFFF", lightened: "FFFFFF", lightenedTwo: "FFFFFF", bitMask: 0b10000)

    static let allTypes = [blue, green, orange, yellow]
    
    static let fire = getColorType(base: "962818", saturated: 0.2, lightened: 0.1, lightenedTwo: 0.15, bitMask: 0b10000)
    
    private static func getColorType(base: String, saturated: CGFloat, lightened: CGFloat, lightenedTwo: CGFloat, bitMask: UInt32) -> ColorType {
            
        let base = UIColor(hex: base)
        let saturated = base.saturate(withValue: saturated)
        let lightened = base.lighten(byPercent: lightened)
        let lightenedTwo = base.lighten(byPercent: lightenedTwo)
        
        return ColorType(base: base, lightened: lightened, lightenedTwo: lightenedTwo, saturated: saturated, bitMask: bitMask)
    }
    
    private static func getColorType(base: String, saturated: String, lightened: String, lightenedTwo: String, bitMask: UInt32) -> ColorType {
        
        let base = UIColor(hex: base)
        let saturated = UIColor(hex: saturated)
        let lightened = UIColor(hex: lightened)
        let lightenedTwo = UIColor(hex: lightenedTwo)
        
        return ColorType(base: base, lightened: lightened, lightenedTwo: lightenedTwo, saturated: saturated, bitMask: bitMask)
    }
    
    static var allTypesBitMask: UInt32 {
        return blue.bitMask | green.bitMask | orange.bitMask | yellow.bitMask
    }
    
    static func getRandomType(_ types: [ColorType] = allTypes) -> ColorType {
        let randomIndex = arc4random_uniform(UInt32(types.count))
        return types[Int(randomIndex)]
    }
    
    static func getRandomType(_ filter: (ColorType) -> Bool) -> ColorType {
        let types = allTypes.filter(filter)
        return getRandomType(types)
    }
}
