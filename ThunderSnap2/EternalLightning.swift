//
//  EternalThread.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 3/31/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class EternalLightning: SKNode {
    var startPoint: CGPoint
    var endPoint: CGPoint
    var interval: CGFloat
    var speedFactor: CGFloat
    var color: UIColor
    var isRandomColor: Bool
    var colorLightDegree: Int
    var sway: CGFloat
    
    init(startPoint: CGPoint, endPoint: CGPoint, interval: CGFloat, speedFactor: CGFloat = 1,
         color: UIColor = .white, isRandomColor: Bool = false, colorLightDegree: Int = 2, sway: CGFloat = 50) {
        
        self.startPoint = startPoint
        self.endPoint = endPoint
        self.interval = interval
        self.speedFactor = speedFactor
        self.color = color
        self.isRandomColor = isRandomColor
        self.colorLightDegree = colorLightDegree
        self.sway = sway
        
        super.init()
        
        run(SKAction.repeatForever(SKAction.sequence([
            SKAction.run {
                if self.isRandomColor {
                    
                    if self.colorLightDegree == 0 {
                        self.color = Category.getRandomType().saturated
                    } else if self.colorLightDegree == 1 {
                        self.color = Category.getRandomType().lightened
                        if self.color == Category.orange.lightened { self.color = Category.orange.lightenedTwo }
                    } else {
                        self.color = Category.getRandomType().lightenedTwo
                    }
                }
                
                let newThread = Lightning(startPoint: self.startPoint, endPoint: self.endPoint,
                                       color: self.color, speedFactor: self.speedFactor, sway: self.sway)
                
                self.addChild(newThread)
            },
            SKAction.wait(forDuration: TimeInterval(interval))])))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
