void main()
{
    vec2 p = -1.0 + 2.0 * v_tex_coord;
    float len = length(p);
    vec2 uv = v_tex_coord - (p/len)*(sin(len*22.-u_time*5.28) + 1.)*0.1;
    
    gl_FragColor = texture2D(u_texture, uv) * u_color;
}
