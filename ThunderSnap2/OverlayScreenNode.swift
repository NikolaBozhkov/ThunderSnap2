//
//  OverlayScreenNode.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 4/13/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class OverlayScreenNode: SKNode {
    
    let fadeDuration: TimeInterval = 0.5
    let padding: CGFloat = 20
    let titleFontSize: CGFloat = 100
    var titleYPos: CGFloat?
    
    let closeBtn: SwirlButton
    let background: SKSpriteNode
    
    init(size: CGSize) {
        background = SKSpriteNode(color: Constants.overlayColor, size: size)
        background.alpha = 0.95
        
        closeBtn = SwirlButton(iconTexture: Resources.shared.closeIconTexture, radius: 50)
        closeBtn.position = CGPoint(x: -size.width / 2 + closeBtn.radius + padding,
                                    y: size.height / 2 - closeBtn.radius - padding)
        
        super.init()
        
        self.isUserInteractionEnabled = true
        self.zPosition = 1000
        
        self.addChild(background)
        //self.addChild(closeBtn)
        
        // Reveal the node with fade on adding to parent
        self.alpha = 0
        let fadeIn = SKAction.fadeIn(withDuration: fadeDuration)
        fadeIn.timingMode = .easeOut
        self.run(fadeIn)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setTitle(_ text: String, yFromTop: CGFloat = 100) {
        let title = SKLabelNode(fontNamed: Constants.mainFont)
        title.fontSize = titleFontSize
        title.horizontalAlignmentMode = .center
        title.verticalAlignmentMode = .center
        title.text = text
        title.position = CGPoint(x: 0, y: background.frame.height / 2 - yFromTop)
        
        self.titleYPos = title.position.y
        
        self.addChild(title)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //let location = touches.first!.location(in: self)
        
            //SoundManager.playSFXAction(Resources.shared.btnClickSFXAction, node: self.parent!)
            
        // Close the overlay by removing it with fade out
        close()
    }
    
    func close() {
        let fadeOut = SKAction.fadeOut(withDuration: fadeDuration)
        fadeOut.timingMode = .easeIn
        
        self.run(SKAction.sequence([
            fadeOut,
            SKAction.removeFromParent()]))
    }
}
