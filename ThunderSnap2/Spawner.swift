//
//  Spawner.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 12/31/16.
//  Copyright © 2016 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class Spawner {
    static let bonusSpawnInterval: TimeInterval = 9
    static let defaultSpawnInterval: TimeInterval = 1.8
    
    unowned let scene: MainScene
    var spawnInterval: TimeInterval = Spawner.defaultSpawnInterval
    var spawnTimePassed: TimeInterval
    var isPaused = false
    var prevType: ColorType?
    var sameTypeInRollCount = 0
    
    init(scene: MainScene) {
        self.scene = scene
        spawnTimePassed = spawnInterval
    }
    
    func update(deltaT: TimeInterval) {
        guard !isPaused else { return }
        
        spawnTimePassed += deltaT
        
        if spawnTimePassed >= spawnInterval {
            spawnCircle()
            self.spawnTimePassed = 0
        }
    }
    
    func spawnStartingCircles() {
        var remainingTypes = Category.allTypes
        
        func randomCircle(at point: CGPoint) {
            let index = Int(arc4random_uniform(UInt32(remainingTypes.count)))
            let type = remainingTypes[index]
            remainingTypes.remove(at: index)
            add(circle: Circle(position: point, colorType: type))
        }
        
        let marginX: CGFloat = 200
        let marginY: CGFloat = 150
        
        // In the 4 corners
        randomCircle(at: CGPoint(x: marginX, y: Constants.gameAreaHeight - marginY))
        randomCircle(at: CGPoint(x: Constants.gameAreaWidth - marginX, y: Constants.gameAreaHeight - marginY))
        randomCircle(at: CGPoint(x: marginX, y: marginY))
        randomCircle(at: CGPoint(x: Constants.gameAreaWidth - marginX, y: marginY))
        
        // In the center
        remainingTypes = Category.allTypes
        randomCircle(at: CGPoint(x: Constants.gameAreaWidth / 2, y: Constants.gameAreaHeight / 2))
    }
    
    func spawnCircle() {
        let position = getPosition()
        
        let type = getColorType()
        let circle = Circle(position: position, colorType: type)
        spawn(circle: circle)
    }
    
    func spawn(circle: Circle, isDelayed: Bool = true) {
        if isDelayed {
            addSpawnNotification(position: circle.position)
            scene.run(SKAction.sequence([
                SKAction.wait(forDuration: 4),
                SKAction.run {
                    self.add(circle: circle)
                }]))
        }
        else {
            add(circle: circle)
        }
    }
    
    private func add(circle: Circle) {
        circle.delegate = scene
        scene.circles.insert(circle)
        scene.gameNode.addChild(circle)
    }
    
    private func addSpawnNotification(position: CGPoint) {
        let danger = SKSpriteNode(texture: Resources.shared.dangerMarkTexture)
        danger.setScale(0)
        danger.position = position
        danger.zPosition = Constants.dangerZPos
        danger.run(SKAction.sequence([
            Resources.shared.dangerAction,
            SKAction.removeFromParent()]))
        scene.gameNode.addChild(danger)
        SoundManager.playSFXAction(Resources.shared.dangerSFXAction, node: scene.gameNode)
    }
    
    private func getColorType() -> ColorType {
        var type: ColorType!
        
        repeat {
            type = Category.getRandomType()
        } while prevType != nil && prevType == type && sameTypeInRollCount >= 2
        
        if type == prevType {
            sameTypeInRollCount += 1
        } else {
            sameTypeInRollCount = 0
        }
        
        prevType = type
        return type
    }
    
    private func getPosition() -> CGPoint {
        var x: CGFloat = 0
        var y: CGFloat = 0
        var tries = 0
        
        repeat {
            // Random X and Y through the whole screen
            x = CGFloat.random(min: Resources.shared.circleRadius, max: Constants.gameAreaWidth - Resources.shared.circleRadius)
            y = CGFloat.random(min: Resources.shared.circleRadius, max: Constants.gameAreaHeight - Resources.shared.circleRadius)
            tries += 1
        } while (!canSpawnCircle(atPoint: CGPoint(x: x, y: y)) && tries < 1000)
        
        return CGPoint(x: x, y: y)
    }
    
    func canSpawnCircle(atPoint point: CGPoint) -> Bool {
        
        // Prevent overlaps circles
        for circle in scene.circles {
            if point.distanceTo(circle.position) <= Resources.shared.circleRadius * 2 {
                return false
            }
        }
        
        return true
    }
}
