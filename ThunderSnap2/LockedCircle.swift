//
//  LockedCircle.swift
//  Attraction
//
//  Created by Nikola on 19/09/2017.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class LockedCircle: GlowNode {
    
    var remainingTypes = Set(Category.allTypes)
    
    init(position: CGPoint) {
        super.init(texture: Resources.shared.circleBaseTexture,
                   textureGlow: Resources.shared.circleBaseGlowTexture,
                   color: .white, colorGlow: .white)
        
        self.position = position
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
