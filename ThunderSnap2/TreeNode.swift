//
//  TreeNode.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 11/22/16.
//  Copyright © 2016 Nikola Bozhkov. All rights reserved.
//

import Foundation

public class TreeNode<T> {
    public var value: T
    public var children = [TreeNode<T>]()
    public var parent: TreeNode<T>?
    
    init(_ value: T) {
        self.value = value
    }
    
    public func getDepth(maxDepth: Int = 0) -> Int {
        var maxDepth = maxDepth
        
        if children.count == 0 {
            return 0
        }
        
        for child in children {
            let depth = child.getDepth(maxDepth: maxDepth) + 1
  
            if (depth > maxDepth) {
                maxDepth = depth
            }
        }
        
        return maxDepth
    }
}
