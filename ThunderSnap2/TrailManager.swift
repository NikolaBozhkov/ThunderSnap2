//
//  TrailManager.swift
//  ThunderSnap2
//
//  Created by Nikola on 28/03/2018.
//  Copyright © 2018 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class TrailManager {
    static let trailStep: CGFloat = 1.9
    var distanceTravelled: CGFloat = 0
    var lastPosition: CGPoint
    let color: SKColor
    
    init(color: SKColor, position: CGPoint) {
        self.color = color
        lastPosition = position
    }
    
    func update(newPosition: CGPoint, parent: SKNode) {
        let deltaD = lastPosition.distanceTo(newPosition)
        distanceTravelled += deltaD
        
        if distanceTravelled > TrailManager.trailStep {
            drawTrail(parent: parent, delta: newPosition - lastPosition, distance: deltaD, position: lastPosition)
        }
        
        lastPosition = newPosition
    }
    
    private func drawTrail(parent: SKNode, delta: CGPoint, distance: CGFloat, position: CGPoint) {
        let count = Int(round(distance / TrailManager.trailStep))
        
        for i in 0..<count {
            let stepDistance = CGFloat(i) * TrailManager.trailStep
            let sine = delta.y / distance
            let cosine = delta.x / distance
            
            let particle = Trail(texture: Resources.shared.trailTexture,
                                 color: color,
                                 sine: sine, cosine: cosine, speedRange: 10)
            
            let particlePosition = position + CGPoint(x: cosine, y: sine) * stepDistance
            particle.position = particlePosition
            particle.zPosition = Constants.trailZPos
            
            // Randomize scale, rotation
            particle.setScale(CGFloat.random(min: 0.2, max: 0.4))
            particle.zRotation = CGFloat.random(min: 0, max: CGFloat.pi * 2)
            
            parent.addChild(particle)
        }
        
        distanceTravelled -= distance
    }
}
