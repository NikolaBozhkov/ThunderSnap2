//
//  Trail.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 1/5/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class Trail: SKSpriteNode {
    static let FadeOutSpeed: CGFloat = 0.02//0.7
    static let RotationSpeed: CGFloat = 0.3
    
    private var moveSpeed: CGFloat!
    private var sine: CGFloat!
    private var cosine: CGFloat!
    
    init(texture: SKTexture, color: UIColor, sine: CGFloat, cosine: CGFloat, speedRange: CGFloat = 0) {
        let randomRange = CGFloat.random(min: -speedRange / 2, max: speedRange / 2)
        self.sine = sine
        self.cosine = cosine
        
        super.init(texture: texture, color: color, size: texture.size())
        self.colorBlendFactor = 1
        self.blendMode = .add
        
        self.run(SKAction.move(by: CGVector(dx: cosine * randomRange,
                                            dy: sine * randomRange),
                               duration: Resources.shared.trailAction.duration))
        self.run(SKAction.sequence([
            Resources.shared.trailAction,
            SKAction.removeFromParent()]))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
