//
//  Circle.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 10/29/16.
//  Copyright © 2016 Nikola Bozhkov. All rights reserved.
//

import Foundation
import SpriteKit

class Circle: SKSpriteNode {
    private static let lightningInterval: TimeInterval = 0.13
    private static let lightningActionKey = "LightningAction"
    private static let deltaForceFactor: CGFloat = 100
    static let strokeWidth: CGFloat = 4
    
    let trailManager: TrailManager
    var followers = Set<Circle>()
    var colorType: ColorType
    var target: Circle?
    var delegate: CircleDelegate?
    
    var radius: CGFloat {
        return self.size.width / 2
    }
    
    init(position: CGPoint, colorType: ColorType = Category.getRandomType()) {
        self.colorType = colorType
        self.trailManager = TrailManager(color: colorType.saturated, position: position)
        
        super.init(texture: Resources.shared.circleBaseTexture,
                   color: colorType.lightened,
                   size: Resources.shared.circleBaseTexture.size())
        self.position = position
        colorBlendFactor = 1
        
        let glow = SKSpriteNode(texture: Resources.shared.circleBaseGlowTexture,
                                color: colorType.saturated,
                                size: Resources.shared.circleBaseGlowTexture.size())
        glow.colorBlendFactor = 1
        glow.position = .zero
        glow.zPosition = Constants.glowZPos
        let duration: TimeInterval = 0.5
        glow.run(SKAction.repeatForever(SKAction.sequence([
            SKAction.fadeAlpha(to: 0.7, duration: duration),
            SKAction.fadeAlpha(to: 0.9, duration: duration)])))
        addChild(glow)
        
        physicsBody = SKPhysicsBody(circleOfRadius: radius)
        physicsBody!.isDynamic = true
        physicsBody!.restitution = 0.1
        physicsBody!.linearDamping = 0.9999999999
        physicsBody!.allowsRotation = false
        // Set collision with everyone except the same color
        physicsBody!.categoryBitMask = colorType.bitMask
        physicsBody!.collisionBitMask = Category.allTypesBitMask ^ colorType.bitMask
        physicsBody!.contactTestBitMask = Category.allTypesBitMask ^ colorType.bitMask
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder: NSCoder) not implemented.")
    }
    
    private func move(deltaT: TimeInterval) {
        guard let target = self.target else { return }
        
        let delta = target.position - position
        let force = delta.normalized() * Circle.deltaForceFactor
        physicsBody!.applyForce(force.vector)
    }
    
    func update(deltaT: TimeInterval) {
        move(deltaT: deltaT)
    }
    
    func didUpdate() {
        guard let target = self.target else { return }
        trailManager.update(newPosition: position, parent: parent!)
        
        // Snap if close enough
        if position.distanceTo(target.position) <= radius {
            position = target.position
            target.consume(circle: self)
        }
    }
    
    func consume(circle: Circle) {
        // Follow this circle instead of the merged one
        for follower in circle.followers {
            if follower != self {
                follower.target = self
                followers.insert(follower)
            }
        }
        
        // Prevent deletion of both circles
        if target === circle {
            clearTarget()
        }
        
        circle.clearTarget()
        delegate?.didConsume(circle: circle)
    }
    
    func clearTarget() {
        if let target = self.target {
            target.followers.remove(self)
            self.target = nil
            removeAction(forKey: Circle.lightningActionKey)
        }
    }
    
    func set(target: Circle) {
        clearTarget()
        self.target = target
        target.followers.insert(self)
        
        run(SKAction.repeatForever(SKAction.sequence([
            SKAction.run {
                let lightning = Lightning(startPoint: self.position, endPoint: target.position, color: self.colorType.lightenedTwo)
                lightning.zPosition = Constants.lightningZPos
                self.parent!.addChild(lightning)
            },
            SKAction.wait(forDuration: Circle.lightningInterval)])), withKey: Circle.lightningActionKey)
    }
}
