//
//  Thread.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 11/1/16.
//  Copyright © 2016 Nikola Bozhkov. All rights reserved.
//

import Foundation
import SpriteKit

class Lightning: SKNode {
    static let Lifetime: CGFloat = 0.2
    static let StaticTime: CGFloat = 0.05
    
    let startPoint: CGPoint
    let endPoint: CGPoint
    let color: SKColor
    let timeLeft: CGFloat
    
    let speedFactor: CGFloat
    let sway: CGFloat
    
    init(startPoint: CGPoint, endPoint: CGPoint, color: SKColor, speedFactor: CGFloat = 1, sway: CGFloat = 50) {
        self.startPoint = startPoint
        self.endPoint = endPoint
        self.color = color
        self.timeLeft = Lightning.Lifetime / speedFactor
        self.speedFactor = speedFactor
        self.sway = sway
        
        super.init()
        
        self.position = CGPoint.zero
        self.createBolt()
        
        self.run(SKAction.sequence([
            SKAction.wait(forDuration: TimeInterval(Lightning.StaticTime / speedFactor)),
            SKAction.fadeOut(withDuration: TimeInterval(Lightning.Lifetime / speedFactor)),
            SKAction.removeFromParent()]))
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func createBolt() {
        let delta = self.endPoint - self.startPoint
        let length = delta.length()
        
        // Flip normal to displace from center of the line
        let normalX = delta.y / length
        let normalY = -delta.x / length
        
        var positions: [CGFloat] = [0]
        
        for _ in 0..<Int(length / 14) {
            positions.append(CGFloat(Float(arc4random()) / Float(UINT32_MAX)))
        }
        
        positions.sort()
        positions.append(1)
        // 80 - 0.55
        
        let jaggednessFactor = 0.55 * (sway / 50)
        let jaggedness = jaggednessFactor / sway
        
        var prevPoint = self.startPoint
        var prevDisplacement: CGFloat = 0
        
        for i in 1..<positions.count {
            let pos = positions[i]
            
            // used to prevent sharp angles by ensuring very close positions also have small perpendicular variation.
            let scale = (length * jaggedness) * (pos - positions[i - 1])
            
            // defines an envelope. Points near the middle of the bolt can be further from the central line.
            let envelope = pos > 0.95 ? 20 * (1 - pos) : 1
            
            var displacement = CGFloat.random(min: -sway, max: sway)
            displacement -= (displacement - prevDisplacement) * (1 - scale)
            displacement *= envelope
            
            let endPoint = CGPoint(x: self.startPoint.x + pos * delta.x + displacement * normalX,
                                   y: self.startPoint.y + pos * delta.y + displacement * normalY)
            
            let line = Line(startPoint: prevPoint, endPoint: endPoint, color: self.color, thickness: 2)
            
            self.addChild(line)
            
            prevPoint = endPoint
            prevDisplacement = displacement
        }
    }
}
