//
//  CGVector+Extensions.swift
//  Attraction
//
//  Created by Nikola on 29/09/2017.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import CoreGraphics

extension CGVector {
    var length: CGFloat {
        return sqrt(pow(dx, 2) + pow(dy, 2))
    }
}

