//
//  SwirlButton.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 3/11/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class SwirlButton: SKNode {
    
    let steps: Int = 4
    let radius: CGFloat
    
    let icon: SKSpriteNode
    //var blur: SKSpriteNode?
    
    init(iconTexture: SKTexture,
         radius: CGFloat = Constants.btnRadius,
         iconScale: CGFloat = 1,
         duration: CGFloat = Constants.btnRotationDuration,
         rotation: CGFloat = Constants.btnRotationAngle,
         gap: CGFloat = 0,
         color: UIColor = .white,
         blurTexture: SKTexture? = nil) {
        
        self.radius = radius
        
        icon = SKSpriteNode(texture: iconTexture)
        icon.setScale(iconScale)
        icon.position = .zero
        icon.colorBlendFactor = 1
        icon.color = color
        icon.run(SwirlButton.createRotateForeverAction(rotation: .pi * 2 / 9, duration: TimeInterval(duration)))
        //icon.run(SwirlButton.createPulseForeverAction(amount: 0.1, duration: TimeInterval(duration)))
        
        // Blur texture
//        if let blurTexture = blurTexture {
//            blur = SKSpriteNode(texture: blurTexture)
//            blur!.position = .zero
//            blur!.zPosition = -1
//            blur!.run(SwirlButton.createGlowForeverAction(glowAmount: 1, duration: TimeInterval(duration)))
////            blur!.color = color
////            blur!.colorBlendFactor = 1
//            //icon.addChild(blur!)
//        }
        
        super.init()
        self.addChild(icon)
        
        let step: CGFloat = .pi * 2 / CGFloat(self.steps) - gap
        
        for i in 0..<self.steps {
            let current = .pi / 2 - CGFloat(i) * (step + gap)
            let arc = SKShapeNode()
            arc.strokeColor = color
            arc.alpha = 1
            arc.lineWidth = 6
            //arc.glowWidth = 2
            //arc.alpha = 1
            arc.path = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: current,
                                    endAngle: current - step, clockwise: false).cgPath
            arc.lineCap = .round
            arc.position = .zero
            
            self.addChild(arc)
            
            // Rotate
            arc.run(SwirlButton.createRotateForeverAction(rotation: rotation, duration: TimeInterval(duration)))
            
            // Resize fragment arcs
            let shrink = SKAction.customAction(withDuration: TimeInterval(duration), actionBlock: {(arc, timePassed) in
                let arc = arc as! SKShapeNode
                
                arc.path = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: current - step * timePassed / duration,
                                        endAngle: current - step, clockwise: false).cgPath
            })
            
            let expand = SKAction.customAction(withDuration: TimeInterval(duration), actionBlock: {(arc, timePassed) in
                let arc = arc as! SKShapeNode
                arc.path = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: (current - step) + step * timePassed / duration,
                                        endAngle: current - step, clockwise: false).cgPath
            })
            
            shrink.timingMode = .easeInEaseOut
            expand.timingMode = .easeInEaseOut
            
            arc.run(SKAction.repeatForever(SKAction.sequence([shrink, expand])))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private class func createRotateForeverAction(rotation: CGFloat, duration: TimeInterval) -> SKAction {
        let rotateLeft = SKAction.rotate(byAngle: rotation, duration: duration)
        let rotateRight = SKAction.rotate(byAngle: -rotation, duration: duration)
        
        rotateLeft.timingMode = .easeInEaseOut
        rotateRight.timingMode = .easeInEaseOut
        return SKAction.repeatForever(SKAction.sequence([rotateLeft, rotateRight]))
    }
    
    private class func createPulseForeverAction(amount: CGFloat, duration: TimeInterval) -> SKAction {
        let scaleUp = SKAction.scale(to: 1 + amount, duration: duration)
        let scaleDown = SKAction.scale(to: 1, duration: duration)
        
        scaleUp.timingMode = .easeInEaseOut
        scaleDown.timingMode = .easeInEaseOut
        return SKAction.repeatForever(SKAction.sequence([scaleUp, scaleDown]))
    }
    
    private class func createGlowForeverAction(glowAmount: CGFloat, duration: TimeInterval) -> SKAction {
        let fadeDown = SKAction.fadeAlpha(by: -glowAmount, duration: duration)
        let fadeUp = SKAction.fadeAlpha(by: glowAmount, duration: duration)
        
        fadeDown.timingMode = .easeInEaseOut
        fadeUp.timingMode = .easeInEaseOut
        return SKAction.repeatForever(SKAction.sequence([fadeDown, fadeUp]))
    }
    
    func setColor(_ color: UIColor) {
        for child in self.children {
            if let arc = child as? SKShapeNode {
                arc.strokeColor = color
            } else if let icon = child as? SKSpriteNode {
                icon.color = color
            }
        }
    }
    
    func setBadge(text: String) {
        let label = SKLabelNode(fontNamed: Constants.mainFont)
        label.verticalAlignmentMode = .center
        label.horizontalAlignmentMode = .center
        label.fontSize = 30
        label.zPosition = 1
        label.position = .zero
        label.text = text
        
        let width = label.frame.size.width + 20
        let height: CGFloat = 50
        let badge = SKShapeNode(rectOf: CGSize(width: width < height ? height : width, height: height), cornerRadius: height / 2)
        badge.fillColor = UIColor.init(hex: "FA2902")
        badge.lineWidth = 2
        badge.addChild(label)
        
        badge.position = CGPoint.init(angle: .pi / 4) * radius
        badge.run(SwirlButton.createPulseForeverAction(amount: 0.15, duration: 0.8))
        
        self.addChild(badge)
    }
}
