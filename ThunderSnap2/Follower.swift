//
//  Follower.swift
//  Attraction
//
//  Created by Nikola on 21/09/2017.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

protocol Follower {
    var target: SKNode? { get set }
}
