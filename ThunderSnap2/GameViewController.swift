//
//  GameViewController.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 10/29/16.
//  Copyright © 2016 Nikola Bozhkov. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import GameKit

class GameViewController: UIViewController, GKGameCenterControllerDelegate {
    
    var gcEnabled = false
    var gcDefaultLeaderboard = String()
    
    let leaderboardId = "com.score.thundersnap"
    
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(showLeaderboard), name: .showLeaderboard, object: nil)

        Resources.load()
        self.presentScene()
    }
    
    @objc func showLeaderboard() {
        GameCenterManager.showLeaderboard(viewController: self)
    }
    
    func presentScene() {
        if let view = self.view as! SKView? {
            
            // Setup view sizes
            Constants.viewWidth = view.frame.size.width
            Constants.viewHeight = view.frame.size.height
            Constants.fullScreenSize = CGSize(width: 1334, height: 1334 * Constants.viewHeight / Constants.viewWidth)
            
            let scene = Util.loadScene(HomeScene(), view: view)

            view.ignoresSiblingOrder = true
            view.showsFPS = false
            view.showsNodeCount = false
        
            view.presentScene(scene)
//            SoundManager.shared.shouldPlay = true
//            SoundManager.shared.bgAudioPlayer?.play()
        }
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
