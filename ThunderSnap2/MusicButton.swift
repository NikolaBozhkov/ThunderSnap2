//
//  MusicButton.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 4/14/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class MusicButton: BlockButton {
    
    let musicOnString = "MUSIC ON"
    let musicOffString = "MUSIC OFF"
    
    init(size: CGSize, fontSize: CGFloat = BlockButton.defaultFontSize, cornerRadius: CGFloat = BlockButton.defaultCornerRadius) {
        super.init(text: "", size: size, fontSize: fontSize, cornerRadius: cornerRadius)
        update()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func toggle() {
        SoundManager.shared.isMusicEnabled = !SoundManager.shared.isMusicEnabled
        update()
    }
    
    func update() {
        if SoundManager.shared.isMusicEnabled {
            self.label.text = self.musicOnString
            self.fillColor = .white
        } else {
            self.label.text = self.musicOffString
            self.fillColor = .gray
        }
    }
}
