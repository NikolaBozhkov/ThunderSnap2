//
//  ScoreHandler.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 12/31/16.
//  Copyright © 2016 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class StatsNode: SKNode {
    static let Points: Int = 1
    static let ComboTimeLimit: TimeInterval = 2
    static let ComboDuration: TimeInterval = 6
    static let ComboTimeLimitMin: TimeInterval = 0.8
    
    let scoreLabel: SKLabelNode!
    let comboLabel: SKLabelNode!
    //let comboCounterLabel: SKLabelNode!
    
    var multiplier = 1
    
    var comboTimeLeft: TimeInterval = 0
    var timeSinceLastMerge: TimeInterval = StatsNode.ComboTimeLimit
    var mergeHistory: [ColorType] = []
    
    var score: Int = 0 {
        didSet {
            scoreLabel.text = String(score)
            // Animations
        }
    }
    
    var bestCombo: Int = 0
    
    var comboCounter: Int = 1 {
        didSet {
            if comboCounter == 1 {
                comboLabel.isHidden = true
                //comboCounterLabel.isHidden = true
            } else {
                //pop(label: comboLabel)
                
                // Increase the font for bigger combos
                comboLabel.fontSize = CGFloat(50 + comboCounter * 2)
                comboLabel.fontSize.clamp(50, 60)
                comboLabel.text = "Combo x" + String(comboCounter)
                pop(label: comboLabel)
            }
            
            if comboCounter - 1 > bestCombo {
                bestCombo = comboCounter
            }
        }
    }
    
    override init() {
        self.scoreLabel = SKLabelNode(fontNamed: Constants.mainFont)
        self.scoreLabel.text = "0"
        self.scoreLabel.fontSize = 55
        self.scoreLabel.horizontalAlignmentMode = .center
        self.scoreLabel.verticalAlignmentMode = .top
        self.scoreLabel.position = .zero
        
        self.comboLabel = SKLabelNode(fontNamed: Constants.mainFont)
//        self.comboLabel.text = "Combo"
        self.comboLabel.fontSize = 50
        self.comboLabel.horizontalAlignmentMode = .center
        self.comboLabel.verticalAlignmentMode = .top
        self.comboLabel.position = CGPoint(x: 0, y: -50)
        
//        self.comboCounterLabel = SKLabelNode(fontNamed: Constants.mainFont)
//        self.comboCounterLabel.fontSize = 50
//        self.comboCounterLabel.horizontalAlignmentMode = .left
//        self.comboCounterLabel.position = CGPoint(x: 20, y: -100)
        
        super.init()
        
        self.addChild(self.scoreLabel)
        self.addChild(self.comboLabel)
       // self.addChild(self.comboCounterLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func pop(label: SKLabelNode) {
        label.isHidden = false
        label.setScale(1)
        label.run(SKAction.sequence([
            SKAction.scale(to: 1.2, duration: 0.2),
            SKAction.scale(to: 1, duration: 0.15)]))
    }
    
    func didCircleMerge(_ circle: Circle) {
        // Check for combo
//        var comboTimeLimit = StatsNode.ComboTimeLimit - 0.05 * TimeInterval(self.comboCounter)
//        if comboTimeLimit < 1 { comboTimeLimit = 1 }
//        
//        if (self.timeSinceLastMerge < comboTimeLimit || circle.isOverloaded) {
//            self.comboCounter += 1
//            self.comboTimeLeft = StatsNode.ComboDuration
//        }
//        
//        let points = StatsNode.Points * self.multiplier * self.comboCounter
        
        
        self.score += 1
        
        // Text inidicator for the player
//        let label = SKLabelNode(text: "+" + String(points))
//        
//        label.fontName = Constants.mainFont
//        label.fontSize = 40
//        label.position = circle.position.offsetted(dx: 0, dy: circle.radius + 20)
//        label.fontColor = circle.colorType.lightenedTwo
//        label.blendMode = .alpha
//        
//        label.run(SKAction.sequence([
//            Resources.shared.scoreAction,
//            SKAction.removeFromParent()]))
//        
//        circle.parent!.addChild(label)
//        
//        self.mergeHistory.append(circle.colorType)
//        self.timeSinceLastMerge = 0
//        
//        // Add Merge for skill point
//        GameData.sharedInstance.currentMergesForSkillPoint += 1
//        if GameData.sharedInstance.currentMergesForSkillPoint >= Constants.mergesForSkillPoint {
//            GameData.sharedInstance.currentMergesForSkillPoint = 0
//            GameData.sharedInstance.skillPoints += 1
//            
//            // Visual indicator
//            let skillPointLabel = SKLabelNode(fontNamed: Constants.mainFont)
//            skillPointLabel.horizontalAlignmentMode = .right
//            skillPointLabel.verticalAlignmentMode = .center
//            skillPointLabel.fontSize = 85
//            skillPointLabel.zPosition = Constants.announcementZPos
//            skillPointLabel.text = "+1"
//            skillPointLabel.position = CGPoint(x: Constants.gameAreaWidth / 2, y: Constants.gameAreaHeight / 2)
//            
//            let icon = SKSpriteNode(texture: Resources.shared.skillPointIconTexture)
//            icon.position = CGPoint(x: icon.frame.width / 2 + 10, y: 0)
//            skillPointLabel.addChild(icon)
//            
//            
//            skillPointLabel.run(SKAction.sequence([
//                Resources.shared.scoreAction,
//                SKAction.removeFromParent()]))
//            
//            circle.parent!.addChild(skillPointLabel)
//        }
    }
    
    func update(deltaT: TimeInterval) {
        // Handle score timers and combos
        self.timeSinceLastMerge += deltaT
        self.comboTimeLeft -= deltaT
        
        if self.comboTimeLeft <= 0 {
            self.comboCounter = 1
        }
    }
    
    func getEndGameStatsModel() -> EndGameStatsModel {
        return EndGameStatsModel(score: self.score, biggestCombo: self.bestCombo)
    }
}
