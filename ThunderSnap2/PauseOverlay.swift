//
//  PauseOverlay.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 4/14/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class PauseOverlay: OverlayScreenNode {
    
    let homeBtn: SwirlButton
    let playBtn: SwirlButton
    let musicBtn: MusicButton
    let sfxBtn: SFXButton
    
    let contentNode: SKNode
    
    override init(size: CGSize) {
        
        let btnSize = CGSize(width: 400, height: 100)
        let btnFontSize: CGFloat = 60
        
        homeBtn = SwirlButton(iconTexture: Resources.shared.homeIconTexture, radius: 60, iconScale: 0.7)
        playBtn = SwirlButton(iconTexture: Resources.shared.playIconTexture, radius: 100)
        musicBtn = MusicButton(size: btnSize, fontSize: btnFontSize)
        sfxBtn = SFXButton(size: btnSize, fontSize: btnFontSize)
        
        contentNode = SKNode()
        super.init(size: size)
        
        setTitle("PAUSE")
        
        homeBtn.position = CGPoint(x: -size.width / 2 + homeBtn.radius + 50, y: titleYPos!)
        
        let margin: CGFloat = 30
        playBtn.position = CGPoint(x: 0, y: -playBtn.radius)
        musicBtn.position = CGPoint(x: 0, y: playBtn.position.y - playBtn.radius - musicBtn.frame.height / 2 - margin)
        sfxBtn.position = CGPoint(x: 0, y: musicBtn.position.y - musicBtn.frame.height / 2 - sfxBtn.frame.height / 2 - margin)
        
        contentNode.addChild(playBtn)
        contentNode.addChild(musicBtn)
        contentNode.addChild(sfxBtn)
        
        contentNode.position = CGPoint(x: 0, y: -(sfxBtn.position.y + sfxBtn.frame.height / 2) / 2)
        
        self.addChild(contentNode)
        self.addChild(homeBtn)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let contentLocation = touches.first!.location(in: self.contentNode)
        let location = touches.first!.location(in: self)
        var btnPressed = true
        
        if homeBtn.contains(location) {
            let scene = self.parent! as! SKScene
            SoundManager.playSFXAction(Resources.shared.btnClickSFXAction, node: scene)
            btnPressed = false
            Util.presentScene(HomeScene(), view: scene.view!)
            
        } else if playBtn.contains(contentLocation) {
            NotificationCenter.default.post(name: .resumeGame, object: nil)
            close()
        } else if musicBtn.contains(contentLocation) {
            musicBtn.toggle()
        } else if sfxBtn.contains(contentLocation) {
            sfxBtn.toggle()
        }
        
        if btnPressed {
            SoundManager.playSFXAction(Resources.shared.btnClickSFXAction, node: self)
        }
    }
}
