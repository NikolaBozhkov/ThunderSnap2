//
//  Double+Extensions.swift
//  Attraction
//
//  Created by Nikola on 27/08/2017.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import Foundation

public extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
