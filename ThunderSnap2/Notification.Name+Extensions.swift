//
//  Notification.Name+Extensions.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 4/1/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let currencyChanged = Notification.Name("currencyChanged")
    static let skillPointsChanged = Notification.Name("skillPoints")
    
    static let showLeaderboard = Notification.Name("showLeaderboard")
    
    static let showInterstitial = Notification.Name("showInterstitialAd")
    static let loadInterstitial = Notification.Name("loadInterstitial")
    static let showRewardedVideo = Notification.Name("showRewardedVideo")
    static let rewardedVideoDidReward = Notification.Name("rewardedVideoDidReward")
    static let loadRewardedVideo = Notification.Name("loadRewardedVideo")
    static let rewardedVideoDidLoad = Notification.Name("rewardedVideoDidLoad")
    static let failedToLoadRewardedVideo = Notification.Name("failedToLoadRewardedVideo")
    static let showFailedToLoadAlert = Notification.Name("showFailedToLoadAlert")
    
    static let resumeGame = Notification.Name("resumeGame")
}
