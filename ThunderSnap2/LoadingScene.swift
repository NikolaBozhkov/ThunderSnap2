//
//  LoadingScene.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 4/14/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class LoadingScene: SKScene {
    
    override func didMove(to view: SKView) {
        let bg = SKSpriteNode(color: Constants.bgColor, size: frame.size)
        bg.anchorPoint = .zero
        bg.position = self.pointFromBottom(.zero)
        bg.zPosition = 1000
        self.addChild(bg)
        
        let loadingLabel = SKLabelNode(fontNamed: Constants.mainFont)
        loadingLabel.horizontalAlignmentMode = .left
        loadingLabel.verticalAlignmentMode = .center
        loadingLabel.fontSize = 100
        loadingLabel.zPosition = 1001
        loadingLabel.text = "Loading..."
        loadingLabel.position = CGPoint(x: frame.midX - loadingLabel.frame.width / 2, y: frame.midY)
        self.addChild(loadingLabel)
        
        var loadingCount = 1
        loadingLabel.run(SKAction.repeatForever(SKAction.sequence([
            SKAction.run {
                loadingLabel.text = "Loading" + String(repeating: ".", count: loadingCount % 4 == 0 ? 1 : loadingCount % 4)
                loadingCount += 1
            },
            SKAction.wait(forDuration: 0.5)])))
        
        self.run(SKAction.sequence([
            SKAction.wait(forDuration: 2),
            SKAction.run {
                SoundManager.shared.shouldPlay = true
                SoundManager.shared.bgAudioPlayer!.play()
                Util.presentScene(HomeScene(), view: self.view!)
            }]))
    }
}
