//
//  Resources.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 12/17/16.
//  Copyright © 2016 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class Resources {
    
    static let shared = Resources()
    
    let selectionAuraEmitter: SKEmitterNode
    
    let lineTexture: SKTexture
    let lineCapTexture: SKTexture
    
    let circleBaseTexture: SKTexture
    let circleBaseGlowTexture: SKTexture
    let selectionAuraTexture: SKTexture
    
    let fireCircleBaseTexture: SKTexture
    let fireCircleBaseGlowTexture: SKTexture
    
    let dangerMarkTexture: SKTexture
    let dangerAction: SKAction
    
    let trailTexture: SKTexture
    let trailAction: SKAction
    
    let scoreAction: SKAction
    
    let preSpawnAuraEmitter: SKEmitterNode
    let circleCoreEmitter: SKEmitterNode
    
    let circleExplosionEmitter: SKEmitterNode
    
    let explosionEmitter: SKEmitterNode
    let sparksEmitter: SKEmitterNode
    let ignitionEmitter: SKEmitterNode
    
    let fireEmitter: SKEmitterNode
    let fireAuraEmitter: SKEmitterNode
    
    // Icons
    let replayIconTexture: SKTexture
    let homeIconTexture: SKTexture
    let skillsIconTexture: SKTexture
    let playIconTexture: SKTexture
    let gameCenterIconTexture: SKTexture
    let upgradeIconTexture: SKTexture
    let colorDiskIconTexture: SKTexture
    let videoIconTexture: SKTexture
    let infoIconTexture: SKTexture
    let settingsIconTexture: SKTexture
    let closeIconTexture: SKTexture
    let skillPointIconTexture: SKTexture
    
    // Title
    let titleBaseTexture: SKTexture
    let titleGlowTexture: SKTexture
    
    // Sounds
    let circleSelectSFXAction: SKAction
    let circleTargetSFXAction: SKAction
    let dangerSFXAction: SKAction
    let explosionSFXAction: SKAction
    let gameOverSFXAction: SKAction
    let igniteSFXAction: SKAction
    let btnClickSFXAction: SKAction
    let mergeSFXAction: SKAction
    let overloadSFXAction: SKAction
    let sparksSFXAction: SKAction
    
    let circleRadius: CGFloat
    
    private init() {
        lineTexture = SKTexture(imageNamed: "line")
        lineCapTexture = SKTexture(imageNamed: "lineCap")
        
        circleBaseTexture = SKTexture(imageNamed: "circleBase")
        circleBaseGlowTexture = SKTexture(imageNamed: "circleBaseGlow")
        selectionAuraTexture = SKTexture(imageNamed: "selectionAura")
        
        fireCircleBaseTexture = SKTexture(imageNamed: "fireCircleBase-1")
        fireCircleBaseGlowTexture = SKTexture(imageNamed: "fireCircleBaseGlow-1")
        
        dangerMarkTexture = SKTexture(imageNamed: "danger")
        dangerAction = SKAction(named: "dangerAction")!
        
        trailTexture = SKTexture(imageNamed: "trail")
        trailAction = SKAction(named: "TrailAction")!
        
        scoreAction = SKAction(named: "ScoreAction")!
        
        preSpawnAuraEmitter = SKEmitterNode(fileNamed: "PreSpawnAura.sks")!
        circleCoreEmitter = SKEmitterNode(fileNamed: "TriangleSwirlCore.sks")!
        selectionAuraEmitter = SKEmitterNode(fileNamed: "TrailBall.sks")!
        
        circleExplosionEmitter = SKEmitterNode(fileNamed: "CircleExplosion.sks")!
        
        explosionEmitter = SKEmitterNode(fileNamed: "Explosion.sks")!
        sparksEmitter = SKEmitterNode(fileNamed: "Sparks.sks")!
        ignitionEmitter = SKEmitterNode(fileNamed: "Ignition.sks")!
        
        fireEmitter = SKEmitterNode(fileNamed: "Fire.sks")!
        fireAuraEmitter = SKEmitterNode(fileNamed: "FireAura.sks")!
        
        // Icons
        replayIconTexture = SKTexture(imageNamed: "replayIcon")
        homeIconTexture = SKTexture(imageNamed: "homeIcon")
        skillsIconTexture = SKTexture(imageNamed: "skillsIcon")
        playIconTexture = SKTexture(imageNamed: "playIcon")
        gameCenterIconTexture = SKTexture(imageNamed: "gameCenterIcon")
        upgradeIconTexture = SKTexture(imageNamed: "upgradeIcon")
        colorDiskIconTexture = SKTexture(imageNamed: "colorDiskIcon")
        videoIconTexture = SKTexture(imageNamed: "videoIcon")
        settingsIconTexture = SKTexture(imageNamed: "settingsIcon")
        infoIconTexture = SKTexture(imageNamed: "infoIcon")
        closeIconTexture = SKTexture(imageNamed: "closeIcon")
        skillPointIconTexture = SKTexture(imageNamed: "skillPointIcon")
        
        // Title
        titleBaseTexture = SKTexture(imageNamed: "titleBase")
        titleGlowTexture = SKTexture(imageNamed: "titleGlow")
        
        // Sounds
        circleSelectSFXAction = SKAction.playSoundFileNamed("circleSelect.caf", waitForCompletion: false)
        circleTargetSFXAction = SKAction.playSoundFileNamed("circleTarget.caf", waitForCompletion: false)
        dangerSFXAction = SKAction.playSoundFileNamed("dangerSpawn.caf", waitForCompletion: false)
        explosionSFXAction = SKAction.playSoundFileNamed("explosion.caf", waitForCompletion: false)
        gameOverSFXAction = SKAction.playSoundFileNamed("gameOver.caf", waitForCompletion: false)
        igniteSFXAction = SKAction.playSoundFileNamed("ignite.caf", waitForCompletion: false)
        btnClickSFXAction = SKAction.playSoundFileNamed("btnClick1.caf", waitForCompletion: false)
        mergeSFXAction = SKAction.playSoundFileNamed("merge.caf", waitForCompletion: false)
        overloadSFXAction = SKAction.playSoundFileNamed("overload.caf", waitForCompletion: false)
        sparksSFXAction = SKAction.playSoundFileNamed("sparks.caf", waitForCompletion: false)
        
        circleRadius = circleBaseTexture.size().width / 2
    }
    
    class func load() {
        let shared = Resources.shared // Loads the init
        
        let label = SKLabelNode(fontNamed: Constants.mainFont)
        label.text = "preload"
        
        shared.circleExplosionEmitter.particleColorSequence = nil
    }
}
