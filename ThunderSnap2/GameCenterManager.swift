//
//  GameCenterManager.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 4/8/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import GameKit

class GameCenterManager {
    
    static let sharedInstance = GameCenterManager()
    
    var gcEnabled = false
    var gcDefaultLeaderboard = String()
    
    let scoreLeaderboardId = "com.score.thundersnap"
    let comboLeaderboardId = "com.combo.thundersnap"
    var authViewController: UIViewController?
    
    private init() {
        
    }
    
    class func setLocalPlayerAuthentication(viewController: UIViewController) {
        let localPlayer = GKLocalPlayer.localPlayer()
        
        localPlayer.authenticateHandler = {(authViewController, error) in
            
            // Present log in
            if authViewController != nil {
                sharedInstance.authViewController = authViewController
                
            } else if localPlayer.isAuthenticated {
                sharedInstance.gcEnabled = true
                
                // If score not up to date and not 0, then update
                if !UserDefaults.standard.bool(forKey: Keys.gcScoreUpToDate) && GameData.sharedInstance.highscore != 0 {
                    reportHighscore(GameData.sharedInstance.highscore)
                }
                
                // If combo not up to date and not 0, then update
                if !UserDefaults.standard.bool(forKey: Keys.gcComboUpToDate) && GameData.sharedInstance.biggestCombo != 0 {
                    reportBiggestCombo(GameData.sharedInstance.highscore)
                }
                
                localPlayer.loadDefaultLeaderboardIdentifier(completionHandler: { (leaderboardIdentifier, error) in
                    if error == nil {
                        sharedInstance.gcDefaultLeaderboard = leaderboardIdentifier!
                        
                    } else {
                        print(error!)
                    }
                })
            } else {
                sharedInstance.gcEnabled = false
            }
        }
    }
    
    private class func reportScore(_ score: Int, upToDateKey: String, leaderboardId: String) {
        guard sharedInstance.gcEnabled else {
            UserDefaults.standard.set(false, forKey: upToDateKey)
            return
        }
       
        let gkBestScore = GKScore(leaderboardIdentifier: leaderboardId)
        gkBestScore.value = Int64(score)
        
        GKScore.report([gkBestScore], withCompletionHandler: { (error) in
            if error != nil {
                print(error!)
                UserDefaults.standard.set(false, forKey: upToDateKey)
            } else {
                UserDefaults.standard.set(true, forKey: upToDateKey)
            }
        })
    }
    
    class func reportHighscore(_ score: Int) {
        reportScore(GameData.sharedInstance.highscore,
                    upToDateKey: Keys.gcScoreUpToDate,
                    leaderboardId: sharedInstance.scoreLeaderboardId)
    }
    
    class func reportBiggestCombo(_ biggestCombo: Int) {
        reportScore(GameData.sharedInstance.biggestCombo,
                    upToDateKey: Keys.gcComboUpToDate,
                    leaderboardId: sharedInstance.comboLeaderboardId)
    }
    
    class func showLeaderboard<T: UIViewController>(viewController: T) where T: GKGameCenterControllerDelegate {
        if sharedInstance.gcEnabled {
            let gcVC = GKGameCenterViewController()
            gcVC.gameCenterDelegate = viewController
            gcVC.viewState = .leaderboards
            gcVC.leaderboardIdentifier = sharedInstance.gcDefaultLeaderboard
            viewController.present(gcVC, animated: true, completion: nil)
        } else if sharedInstance.authViewController != nil {
            viewController.present(sharedInstance.authViewController!, animated: true, completion: nil)
        }
    }
}
