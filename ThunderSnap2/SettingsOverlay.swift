//
//  SettingsOverlay.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 4/13/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class SettingsOverlay: OverlayScreenNode {
    
    let musicBtn: MusicButton
    let sfxBtn: SFXButton
    
    let contentNode: SKNode
    
    override init(size: CGSize) {
        let btnSize = CGSize(width: 400, height: 100)
        let margin: CGFloat = 30
        let btnFontSize: CGFloat = 60
        
        self.musicBtn = MusicButton(size: btnSize, fontSize: btnFontSize)
        self.musicBtn.position = CGPoint(x: 0, y: -btnSize.height / 2)
        
        self.sfxBtn = SFXButton( size: btnSize, fontSize: btnFontSize)
        self.sfxBtn.position = CGPoint(x: 0, y: musicBtn.position.y - btnSize.height - margin)
        
        contentNode = SKNode()
        super.init(size: size)
        self.setTitle("SETTINGS")
        
        contentNode.addChild(self.musicBtn)
        contentNode.addChild(self.sfxBtn)
        
        let howToPauseLabel = SKLabelNode(fontNamed: Constants.mainFont)
        howToPauseLabel.verticalAlignmentMode = .top
        howToPauseLabel.horizontalAlignmentMode = .center
        howToPauseLabel.fontSize = 50
        howToPauseLabel.text = "Tap with 3 fingers to pause."
        howToPauseLabel.position = CGPoint(x: 0, y: sfxBtn.position.y - btnSize.height / 2 - margin)
        contentNode.addChild(howToPauseLabel)
        
        contentNode.position = CGPoint(x: 0, y: -(howToPauseLabel.position.y - howToPauseLabel.frame.height) / 2)
        
        self.addChild(contentNode)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let location = touches.first!.location(in: contentNode)
        
        var btnPressed = true
        
        if self.musicBtn.contains(location) {
            self.musicBtn.toggle()
        } else if self.sfxBtn.contains(location) {
            self.sfxBtn.toggle()
        } else {
            btnPressed = false
        }
        
        if btnPressed {
            SoundManager.playSFXAction(Resources.shared.btnClickSFXAction, node: self)
        } else {
            super.touchesBegan(touches, with: event)
        }
    }
}
