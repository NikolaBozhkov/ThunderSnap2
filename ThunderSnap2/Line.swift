//
//  Line.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 11/2/16.
//  Copyright © 2016 Nikola Bozhkov. All rights reserved.
//

import Foundation
import SpriteKit

public class Line: SKNode {    
    private static let ImageThickness: CGFloat = 3.0
    
    init(startPoint: CGPoint, endPoint: CGPoint, color: UIColor, thickness: CGFloat = 1) {
        super.init()
        self.position = startPoint
        //self.alpha = 1.0

        let delta = endPoint - startPoint
        let length = delta.length()
        let rotation = delta.angle

        let capLeft = SKSpriteNode(texture: Resources.shared.lineCapTexture)
        capLeft.anchorPoint = CGPoint(x: 1, y: 0.5)
        capLeft.position = CGPoint.zero
        capLeft.color = color
        capLeft.blendMode = .add
        capLeft.colorBlendFactor = 1
        self.addChild(capLeft)
        
        let line = SKSpriteNode(texture: Resources.shared.lineTexture)
        line.anchorPoint = CGPoint(x: 0, y: 0.5)
        line.xScale = length
        line.position = CGPoint.zero
        line.color = color
        line.blendMode = .add
        line.colorBlendFactor = 1
        self.addChild(line)
        
        
        let capRight = SKSpriteNode(texture: Resources.shared.lineCapTexture)
        capRight.anchorPoint = CGPoint(x: 1, y: 0.5)
        capRight.xScale = -1
        capRight.position = CGPoint(x: line.position.x + line.size.width, y: 0)
        capRight.color = color
        capRight.blendMode = .add
        capRight.colorBlendFactor = 1
        self.addChild(capRight)
        
        self.yScale = thickness / Line.ImageThickness
        self.zRotation = rotation
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
