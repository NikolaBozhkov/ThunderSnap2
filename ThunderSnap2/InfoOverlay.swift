//
//  InfoOverlay.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 4/13/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class InfoOverlay: OverlayScreenNode {
    
    let tutorialBtn: BlockButton
    let contentNode: SKNode
    
    override init(size: CGSize) {
        contentNode = SKNode()
        
        tutorialBtn = BlockButton(text: "HOW TO PLAY", size: CGSize(width: 500, height: 100), fontSize: 70)
        tutorialBtn.position = CGPoint(x: 0, y: -tutorialBtn.frame.height / 2)
        
        super.init(size: size)
        
        let createdByLabel = createBaseLabel(text: "Created By", fontSize: 80)
        createdByLabel.position = CGPoint(x: 0, y: tutorialBtn.position.y - tutorialBtn.frame.height / 2 - 50)
        
        // Change color forever
        var lastColor: UIColor = .clear
        var color = Category.getRandomType().saturated
        createdByLabel.run(SKAction.repeatForever(SKAction.sequence([
            SKAction.run {
                color = Category.getRandomType { lastColor.hash != $0.saturated.hash }.saturated
                lastColor = color
                
                createdByLabel.run(SKAction.colorize(with: color, colorBlendFactor: 1, duration: 0.3))
            },
            SKAction.wait(forDuration: 0.3)])))
        
        // Fade in
        let fade = SKAction.fadeIn(withDuration: 0.7)
        let waitAction = SKAction.wait(forDuration: fade.duration / 2)
        fade.timingMode = .linear
        
        let creatorLabel = createBaseLabel(text: "Nikola Bozhkov", fontSize: 80)
        creatorLabel.fontColor = UIColor(hex: "F2F2F2")
        creatorLabel.position = CGPoint(x: 0, y: createdByLabel.position.y - createdByLabel.frame.height - 20)
        
        // Thanks label
        let thanksLabel = createBaseLabel(text: "Thanks to everyone who gave feedback!", fontSize: 65)
        thanksLabel.position = CGPoint(x: 0, y: creatorLabel.position.y - creatorLabel.frame.height - 100)
        let thanksLabel2 = createBaseLabel(text: "GLHF!", fontSize: 65)
        thanksLabel2.position = CGPoint(x: 0, y: thanksLabel.position.y - thanksLabel.frame.height - 20)
        
        func randomPointInCreator() -> CGPoint {
            let extra: CGFloat = 5
            let x = CGFloat.random(min: creatorLabel.position.x - creatorLabel.frame.width / 2 - extra,
                                   max: creatorLabel.position.x + creatorLabel.frame.width / 2 + extra)
            let y = CGFloat.random(min: creatorLabel.position.y - creatorLabel.frame.height - extra,
                                   max: creatorLabel.position.y + extra)
            return CGPoint(x: x, y: y)
        }
        
        self.run(SKAction.sequence([
            SKAction.wait(forDuration: fadeDuration / 2),
            SKAction.run {
                createdByLabel.run(fade)
            },
            waitAction,
            SKAction.run {
                creatorLabel.run(fade)
                
                // Lightning in name effects
                self.run(SKAction.repeatForever(SKAction.sequence([
                    SKAction.run {
                        let start = randomPointInCreator()
                        var end: CGPoint!
                        repeat {
                            end = randomPointInCreator()
                        } while start.distanceTo(end) < 20
                        
                        let thread = Lightning(startPoint: start, endPoint: end, color: Category.getRandomType().lightened, speedFactor: 0.5)
                        thread.zPosition = 1
                        self.contentNode.addChild(thread)
                        
                    },
                    SKAction.wait(forDuration: 0.2)])))
            },
            waitAction,
            SKAction.run {
                thanksLabel.run(fade)
            },
            waitAction,
            SKAction.run {
                thanksLabel2.run(fade)
            }]))
        
        contentNode.position = CGPoint(x: 0, y: -(thanksLabel2.position.y - thanksLabel2.frame.height) / 2)
        contentNode.addChild(tutorialBtn)
        contentNode.addChild(createdByLabel)
        contentNode.addChild(creatorLabel)
        contentNode.addChild(thanksLabel)
        contentNode.addChild(thanksLabel2)
        
        self.addChild(contentNode)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let location = touches.first!.location(in: self.contentNode)
        super.touchesBegan(touches, with: event)
    }
    
    func createBaseLabel(text: String, fontSize: CGFloat) -> SKLabelNode {
        let label = SKLabelNode(fontNamed: Constants.mainFont)
        label.verticalAlignmentMode = .top
        label.horizontalAlignmentMode = .center
        label.fontSize = fontSize
        label.text = text
        label.alpha = 0
        return label
    }
}
