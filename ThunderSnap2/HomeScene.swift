//
//  HomeScene.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 3/18/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class HomeScene: SKScene {
    
    var timeSceneAlive: TimeInterval = 0
    var lastTime: TimeInterval = 0
    var timePassed: TimeInterval = 0.05
    let titleThreadInterval: TimeInterval = 0.05
    
    let threadOffset: CGFloat = 40
    
    let btnYBaseline: CGFloat = 90
    let btnYSide: CGFloat = 430
    let btnMargin: CGFloat = 70
    let playBtnRadius: CGFloat = 110
    
    var playBtn: SwirlButton!
    var skillsBtn: SwirlButton!
    var gameCenterBtn: SwirlButton!
    var statsBtn: SwirlButton!
    var infoBtn: SwirlButton!
    var settingsBtn: SwirlButton!
    
    var isRewardedVideoEnabled = false
    var didRequestRewardedVideo = false
    var rewardedVideoCd: CGFloat = 0
    var serverTime: CGFloat = 0
    
    var title: SKSpriteNode!
    
    override func didMove(to view: SKView) {
        
        let bg = SKSpriteNode(color: Constants.bgColor, size: frame.size)
        bg.anchorPoint = .zero
        bg.position = self.pointFromBottom(.zero)
        bg.zPosition = -100
        self.addChild(bg)
        
        // Buttons
        playBtn = SwirlButton(iconTexture: Resources.shared.playIconTexture, radius: playBtnRadius)
        playBtn.position = self.pointFromBottom(CGPoint(x: frame.midX, y: btnYBaseline + playBtnRadius))
        addChild(playBtn)
        
        gameCenterBtn = SwirlButton(iconTexture: Resources.shared.gameCenterIconTexture)
        gameCenterBtn.position = Util.getButtonPosition(centerBtn: playBtn, number: 1)
        addChild(gameCenterBtn)
        
        infoBtn = SwirlButton(iconTexture: Resources.shared.infoIconTexture)
        infoBtn.position = Util.getButtonPosition(centerBtn: playBtn, number: -2)
        addChild(infoBtn)

        settingsBtn = SwirlButton(iconTexture: Resources.shared.settingsIconTexture)
        settingsBtn.position = Util.getButtonPosition(centerBtn: playBtn, number: 2)
        addChild(settingsBtn)
        
        let lightningEffect = EternalLightning(startPoint: self.pointFromBottom(CGPoint(x: threadOffset, y: threadOffset)),
                                            endPoint: self.pointFromBottom(CGPoint(x: frame.size.width - threadOffset, y: threadOffset)),
                                            interval: 0.2,
                                            speedFactor: 0.5,
                                            isRandomColor: true,
                                            colorLightDegree: 1)
        self.addChild(lightningEffect)
        
        // Title
        title = TitleNode()
        title.position = self.pointFromTop(CGPoint(x: self.frame.midX, y: 40 + title.frame.height / 2))
        //title.anchorPoint = CGPoint(x: 0.5, y: 0)
        
        self.addChild(title)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let location = touches.first!.location(in: self)
        
        var btnPressed = true
        
        if playBtn.contains(location) {
            Util.presentScene(MainScene(), view: self.view!)
        }
        else if gameCenterBtn.contains(location) {
            NotificationCenter.default.post(name: .showLeaderboard, object: nil)
            
        }
        else if infoBtn.contains(location) {
            let infoOverlay = InfoOverlay(size: self.frame.size)
            infoOverlay.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
            self.addChild(infoOverlay)
            
        }
        else if settingsBtn.contains(location) {
            let settingsOverlay = SettingsOverlay(size: self.frame.size)
            settingsOverlay.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
            self.addChild(settingsOverlay)
            
        }
        else {
            btnPressed = false
        }
        
        if btnPressed {
            SoundManager.playSFXAction(Resources.shared.btnClickSFXAction, node: self)
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        let deltaT = lastTime == 0 ? 0 : currentTime - lastTime
        timeSceneAlive += deltaT
        timePassed += deltaT
        
        if timePassed >= titleThreadInterval {
            
            let start = randomPointInTitle()
            var end: CGPoint!
            repeat {
                end = randomPointInTitle()
            } while start.distanceTo(end) < 20
            
            let thread = Lightning(startPoint: start, endPoint: end, color: Category.getRandomType().lightened, speedFactor: 0.5)
            self.addChild(thread)
            
            timePassed = 0
        }
        
        lastTime = currentTime
    }
    
    func randomPointInTitle() -> CGPoint {
        let x = CGFloat.random(min: title.position.x - title.frame.width / 2,
                               max: title.position.x + title.frame.width / 2)
        let y = CGFloat.random(min: title.position.y - title.frame.height / 2,
                               max: title.position.y + title.frame.height / 2)
        return CGPoint(x: x, y: y)
    }
    
    override func willMove(from view: SKView) {
        NotificationCenter.default.removeObserver(self)
    }
}
