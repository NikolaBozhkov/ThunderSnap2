//
//  ColorCircle.swift
//  Attraction
//
//  Created by Nikola on 17/09/2017.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

class ColorCircle: SKSpriteNode {
    
    static let strokeWidth: CGFloat = 4
    
    static let speedFactor: CGFloat = 3100
    static let accelerationSteep: CGFloat = 130
    static let speedModifier: CGFloat = 120
    
    static let coreRotationSpeedDefault: CGFloat = .pi
    static let coreRotationSpeedInitial = ColorCircle.coreRotationSpeedDefault / 2
    
    static let trailStep: CGFloat = 1.9
    static let threadInterval: TimeInterval = 0.13
    
    var trailDistance: CGFloat = 0
    var timeSinceLastThread: TimeInterval = ColorCircle.threadInterval
    var coreRotationSpeed: CGFloat = 0
    
    var isDisabled = true
    
    let colorType: ColorType
    
    let core: SKEmitterNode
    let circle: GlowNode
    var stageParticles: Set<SKSpriteNode> = Set()
    
    var target: ColorCircle?
    
    var radius: CGFloat {
        return self.size.width / 2
    }
    
    var stage: Int = 0 {
        didSet {
            
        }
    }
    
    init(position: CGPoint, colorType: ColorType) {
        
        core = Resources.shared.circleCoreEmitter.copy() as! SKEmitterNode
        core.particleColor = colorType.lightened
        core.zPosition = Constants.corePreSpawnZPos
        core.alpha = 0
        
        circle = GlowNode(texture: Resources.shared.circleBaseTexture,
                          textureGlow: Resources.shared.circleBaseGlowTexture,
                          color: colorType.lightened, colorGlow: colorType.saturated)
        circle.setGlow(from: 0.7, to: 0.9, duration: 0.5)
        
        self.colorType = colorType
        
        super.init(texture: nil, color: .clear, size: Resources.shared.circleBaseTexture.size())
        
        self.position = position
        
        // Hide until spawned
        circle.isHidden = true
        
        addChild(core)
        addChild(circle)
        
        // Set physics body
        physicsBody = SKPhysicsBody(circleOfRadius: radius)
        physicsBody!.isDynamic = false
        physicsBody!.restitution = 0.1
        physicsBody!.linearDamping = 0.9999999999
        
        // Set collision with everyone except the same color
        physicsBody!.categoryBitMask = 0
        physicsBody!.collisionBitMask = Category.allTypesBitMask ^ colorType.bitMask
        physicsBody!.contactTestBitMask = Category.allTypesBitMask ^ colorType.bitMask
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func spawn(delay: Int) {
        // Add an indicating aura that pings every second
        let emitter = Resources.shared.preSpawnAuraEmitter.copy() as! SKEmitterNode
        emitter.zPosition = Constants.preSpawnZPos
        emitter.position = .zero
        emitter.particleColor = colorType.lightened
        
        emitter.run(SKAction.sequence([
            SKAction.wait(forDuration: TimeInterval(delay)),
            SKAction.run {
                emitter.particleBirthRate = 0
            },
            SKAction.wait(forDuration: TimeInterval(emitter.particleLifetime)),
            SKAction.removeFromParent()]))
        
        addChild(emitter)
        
        // Gradually reveal the core
        core.run(SKAction.fadeIn(withDuration: TimeInterval(delay)))
        
        // Progress stroke circle
        let stroke = SKShapeNode(circleOfRadius: radius)
        stroke.position = .zero
        stroke.strokeColor = colorType.lightened
        stroke.lineWidth = 2.5
        stroke.glowWidth = 1
        stroke.zPosition = Constants.corePreSpawnZPos
        stroke.lineCap = .round
        
        addChild(stroke)
        
        stroke.run(SKAction.customAction(withDuration: TimeInterval(delay), actionBlock: {(stroke, timePassed) in
            let stroke = stroke as! SKShapeNode
            
            stroke.path = UIBezierPath(arcCenter: .zero, radius: self.radius - stroke.lineWidth / 2, startAngle: .pi / 2,
                                       endAngle: .pi / 2 - .pi * 2 * timePassed / CGFloat(delay), clockwise: false).cgPath
        }))
        
        
        // Slowly increase the speed of the core to the normal one
        self.run(SKAction.customAction(withDuration: TimeInterval(delay), actionBlock: {(circle, timePassed) in
            let circle = circle as! ColorCircle
            
            // Start from some speed and animate the end value
            circle.coreRotationSpeed = ColorCircle.coreRotationSpeedInitial +
                (ColorCircle.coreRotationSpeedDefault - ColorCircle.coreRotationSpeedInitial) * timePassed / CGFloat(delay)
        }))
        
        self.run(SKAction.sequence([
            SKAction.wait(forDuration: TimeInterval(delay)),
            SKAction.run {
                // Remove the stroke and enable the circle
                stroke.removeFromParent()
                self.spawn()
            }]))
    }
    
    func spawn() {
        isDisabled = false
        circle.isHidden = false
        core.alpha = 1
        core.zPosition = Constants.coreZPos
        core.advanceSimulationTime(TimeInterval(Resources.shared.circleCoreEmitter.particleLifetime))
        coreRotationSpeed = ColorCircle.coreRotationSpeedDefault
        physicsBody!.isDynamic = true
        physicsBody!.categoryBitMask = colorType.bitMask
    }
    
    private func updateLightningThread(deltaT: TimeInterval) {
        timeSinceLastThread += deltaT
        guard let target = self.target else { return }
        
        // Creates a lighning between the current positions of the 2 circles
        if timeSinceLastThread > ColorCircle.threadInterval {
            let thread = Thread(startPoint: position, endPoint: target.position, color: colorType.lightenedTwo)
            thread.zPosition = Constants.threadZPos
            
            parent!.addChild(thread)
            
            timeSinceLastThread = 0
        }
    }
    
    private func drawTrail(moveXY: CGPoint, position: CGPoint) {
        let count = Int(round(trailDistance / ColorCircle.trailStep))
        let trailDrawDistance = CGFloat(count) * ColorCircle.trailStep
        let sin = moveXY.y / trailDrawDistance
        let cos = moveXY.x / trailDrawDistance
        
        for i in 0..<count {
            let stepDistance = CGFloat(i) * ColorCircle.trailStep
            
            let particle = Trail(texture: Resources.shared.trailTexture,
                                 color: colorType.saturated,
                                 sin: sin, cos: cos, speedRange: 10)
            
            particle.position = position + CGPoint(x: cos, y: sin) * stepDistance
            particle.zPosition = Constants.trailZPos
            
            // Randomize scale, rotation
            particle.setScale(CGFloat.random(min: 0.2, max: 0.4))
            particle.zRotation = CGFloat.random(min: 0, max: CGFloat.pi * 2)
            
            parent!.addChild(particle)
        }
        
        trailDistance -= trailDrawDistance
    }
    
    private func addStageParticles(count: Int) {
        
        for _ in 0..<count {
            let particle = SKSpriteNode(texture: Resources.shared.stageParticleTexture)
            particle.blendMode = .add
            particle.color = .white
            particle.zPosition = Constants.stageParticleZPos
            particle.alpha = 0.7
            
            stageParticles.insert(particle)
            addChild(particle)
        }
        
        repositionStageParticles()
    }
    
    private func repositionStageParticles() {
        let angle = CGFloat.pi * 2 / CGFloat(stageParticles.count)
        
        for (i, particle) in stageParticles.enumerated() {
            particle.position = CGPoint(angle: angle * CGFloat(i)) * (radius - ColorCircle.strokeWidth)
        }
    }
    
    private func move(deltaT: TimeInterval) {
        guard let target = target else { return }
        
        let deltaXY = target.position - position
        let distance = target.position.distanceTo(position)
        
        // Move increasingly faster when closer
        let speed = pow(ColorCircle.speedFactor / (distance + ColorCircle.accelerationSteep), 2)
            + ColorCircle.speedModifier
        
        // Move towards target circle
        var moveXY = (deltaXY / distance) * (CGFloat(deltaT) * speed)
        
        // Snap if it's going to skip the target
        moveXY.x = abs(moveXY.x) > abs(deltaXY.x) ? deltaXY.x : moveXY.x
        moveXY.y = abs(moveXY.y) > abs(deltaXY.y) ? deltaXY.y : moveXY.y
        
        // Draw trail based on the distance travelled to make it even
        let distanceMoved = moveXY.length()
        
        trailDistance += distanceMoved
        if trailDistance > ColorCircle.trailStep {
            drawTrail(moveXY: moveXY, position: position)
        }
        
        position.x += moveXY.x
        position.y += moveXY.y
        
        // Stop circle from colliding with target
        
        if let target = target as? ColorCircle,
            colorType.bitMask != target.colorType.bitMask,
            position.distanceTo(target.position) <= Resources.shared.circleRadius * 2 {
            
            position.x -= moveXY.x
            position.y -= moveXY.y
            self.target = nil
        }
    }
    
    func update(deltaT: TimeInterval) {
        core.zRotation -= CGFloat(deltaT) * coreRotationSpeed
        
        move(deltaT: deltaT)
        updateLightningThread(deltaT: deltaT)
    }
    
    func select() {
        circle.color = colorType.lightenedTwo
        core.particleColor = colorType.lightenedTwo
        
        circle.addGlow()
        
        SelectionAuraManager.assignAura(toCircle: self)
        //self.run(Resources.selectSFXAction)
    }
    
    func deselect() {
        circle.color = colorType.lightened
        core.particleColor = colorType.lightened
        
        circle.removeGlow()
        
        SelectionAuraManager.removeAura(fromCircle: self)
        //self.run(Resources.deselectSFXAction)
    }
    
    func isAtTarget() -> Bool {
        guard let target = target else { return false }
        
        return target.position == position
    }
    
    typealias Consumable = ColorCircle
    func consume(_ element: ColorCircle) {
        stage += element.stage + 1
    }
    
    func merge(withCircle circle: ColorCircle) {
        guard let parent = parent else { return }
        
        // Prevent deletion of both circles
        if target is ColorCircle && (target as! ColorCircle) === circle {
            target = nil
        }
        
        // Run explosion effect
        let explosion = Resources.shared.circleExplosionEmitter.copy() as! SKEmitterNode
        explosion.position = position
        explosion.zPosition = Constants.mergeEffectZPos
        explosion.particleColor = colorType.lightened
        
        explosion.run(SKAction.sequence([
            SKAction.wait(forDuration: TimeInterval(explosion.particleLifetime)),
            SKAction.removeFromParent()]))
        parent.addChild(explosion)
        
        SoundManager.playSFXAction(Resources.shared.mergeSFXAction, node: self)
    }
    
    func remove(fromScene scene: MainScene) {
        // Remove connection to scene
        scene.colorCircles.remove(self)
        
        if self === scene.selectedCircle {
            deselect()
            scene.selectedCircle = nil
        }
        
        target = nil
        
        removeFromParent()
    }
}
