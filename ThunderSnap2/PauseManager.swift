//
//  PauseManager.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 4/14/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import Foundation

enum PauseOption: Int {
    case all = 0
    case button
    case threeFingerTap
}

class PauseManager {
    
    static let shared = PauseManager()
    
    var pauseOption: PauseOption {
        didSet {
            UserDefaults.standard.set(pauseOption.rawValue, forKey: Keys.pauseOption)
        }
    }
    
    private init() {
        pauseOption = PauseOption(rawValue: UserDefaults.standard.integer(forKey: Keys.pauseOption))!
    }
}
