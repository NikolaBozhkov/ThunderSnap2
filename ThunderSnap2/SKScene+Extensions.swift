//
//  SKScene+Extensions.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 3/29/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

extension SKScene {
    
    var viewTop: CGFloat {
        return convertPoint(fromView: .zero).y
    }
    
    var viewBottom: CGFloat {
        guard let view = self.view else { return 0 }
        return convertPoint(fromView: CGPoint(x: 0, y: view.bounds.height)).y
    }
    
    func pointFromTop(_ p: CGPoint) -> CGPoint {
        var res = p
        res.y = viewTop - p.y
        return res
    }
    
    func pointFromBottom(_ p: CGPoint) -> CGPoint {
        var res = p
        res.y += viewBottom
        return res
    }
}
