//
//  GameObject.swift
//  Attraction
//
//  Created by Nikola on 23/09/2017.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

protocol GameObject {
    var position: CGPoint { get set }
}
