//
//  Constants.swift
//  Attraction
//
//  Created by Nikola Bozhkov on 3/18/17.
//  Copyright © 2017 Nikola Bozhkov. All rights reserved.
//

import SpriteKit

struct Constants {
    // Z positions
    static let statsNodeZPos: CGFloat = -100
    static let overloadAuraZPos: CGFloat = -50
    static let trailZPos: CGFloat = -30
    static let glowZPos: CGFloat = -20
    static let addedGlowZPos: CGFloat = -19
    static let spinnerZPos: CGFloat = -1
    static let baseZPos: CGFloat = 0
    static let mergeEffectZPos: CGFloat = 6
    static let lightningZPos: CGFloat = 7
    static let overloadRadiusZPos: CGFloat = 10
    static let selectionAuraEmitterZPos: CGFloat = 20
    static let spinnerPreSpawnZPos: CGFloat = 100
    static let preSpawnZPos: CGFloat = 100
    static let dangerZPos: CGFloat = 101
    static let sparksZPos: CGFloat = 200
    static let explosionZPos: CGFloat = 202
    static let announcementZPos: CGFloat = 900
    static let flashScreenZPos: CGFloat = 1000
    
    static let btnRotationAngle = CGFloat.pi * 5 / 2
    static let btnRotationDuration: CGFloat = 1.5
    static let btnMargin: CGFloat = 65
    static let btnRadius: CGFloat = 80
    
    static let mergesForSkillPoint: Int = 100
    
    static var viewWidth: CGFloat!
    static var viewHeight: CGFloat!
    
    static var fullScreenSize: CGSize!

    static let gameAreaHeight: CGFloat = 750
    static let gameAreaWidth: CGFloat = 1334
    
    static let rewardVideoCooldown: CGFloat = 3600
    static let interstitialGamesInterval = 3
    static let rewardVideoGamesInterval = 3
    static let rewardVideoCurrency = 100
    
    static let upgradeColor = UIColor(hex: "90FF28")
    static let highlightColor = UIColor.magenta//UIColor(hex: "DDE0FF")
    static let dangerRedColor = UIColor(hex: "E62933")
    static let bgColor = UIColor(hex: "232326")
    static let overlayColor = UIColor(hex: "121215")
    
    static let mainFont = "Westmeath"
}
